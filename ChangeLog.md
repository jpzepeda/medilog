# Changelog

## v1.9.0.3, build 5319
New 

- Translations (thank you @mondstern@mastodon.technology)
    + Started with spanish translation
    + Started with danish translation
    + Dutch complete
    + Italian complete
    + German complete
    + French complete

Fixed

- Added state highlighting to Diary PDF report. "Not good" = Italics, Bad = bold italics
- Fixed a crash when trying to filter an empty table
- Fixed an issue where the filter icon is not reset after a data import

Known issues

- Water Intake report not (yet) showing summary

## v1.9.0.2, build 5318

New 

- Nothing

Fixed

- Fixed a nasty bug in water tab which would crash the app if no data was compiled for today

Known issues
- Water Intake report not (yet) showing summary
- Diary PDF report not showing state

## v1.9.0.1, build 5317

New 

- Initial support for Dutch language (thank you @mondstern@mastodon.technology)
- Initial support for Italian language (thank you @mondstern@mastodon.technology)
- Finishing off most of the missing water features
- Added Today row to Water intake screen

Fixed

- Nothing

Known issues

- Water Intake settings not (yet) exposed to user
- Water Intake report not (yet) showing summary


## v1.9.0.0 Beta, build 5316

New 

- Added Heart Rhythm logging
- Added Water Intake logging
- Added basic Water Intake report

Fixed

- Fixed bug where Weight edit wouldn't load the comment
- Setting/Changing the filter is now reflected in the data tabs right away.

Known issues

- Water Intake settings not (yet) exposed to user
- Water Intake report not (yet) showing summary
- Water Intake main screen not (yet) showing summary of daily intake


## v1.8.1.3, build 5315

New 

- Nothing

Fixed

- Fixed bug where Weight edit wouldn't load the comment
- Setting/Changing the filter is now reflected in the data tabs right away.

Known issues

- None


## v1.8.1.2, build 5314
New 
- Nothing

Fixed
- Fixed a bug where "send CSV" would lead to a "PDF not found" error message


## v1.8.1.1, build 5313
New 

- Nothing

Fixed

- De-branded screenshots
- Introduced 4th digit in version number to properly reflect bug fix releases for F-Droid

## v1.8.1, build 5311
New 
- Added French tr
anslation, thanks to https://gitlab.com/djelouze
- Added | as acceptable CSV field delimiter

## v1.8.0, build 5310
New

- Added state (good/notGood/bad) / (no colour/amber/red) to Diary tab
- Cleaned up filter dialog
- Accept all valid CSV delimiters during import/restore
- Added restore from ZIP backup file
- Enabled pin authentication as fallback for biometric logon for API levels > 29 (Android 11)

Fixed

- Changed colour of Floating action buttons from black to white to increase contrast
- "Send raw data" crashed when data set was too large.
- Significant re-architecture of the filter section
- Should have fixed a bug where new data added wasn't showing without a ViewModel reload
- Fixed bug which prevent setting , as CSV delimiter
- More secure handling of ZIP password (Hide password, ask for pwd if none i set, etc.)

## v1.7.6, build 5309
New

- Added | as acceptable CSV field delimiter
- Added check for empty CSV field delimiter in settings dialog
- Added pin authentication as fallback for biometric logon (disabled for now until bug https://issuetracker.google.com/issues/142740104 is fixed)
- More delimiter checking and better feedback in case of errors during import

Fixed
- Fixed some documentation inconsistencies in regards to version numbers, build number, etc.
- Disable biometric logon instead of hiding if biometric is not available/set up

## v1.7.5, build 5308
New

- Added Database encryption 
- Added feature to activate first edit field whenever an edit screen is opened, saves one click for each entry :-)
- Added "Show Pulse" setting to further clean up Blood Pressure chart
- Added database size to About screen
- Added number of items exported to backup success messages
- Added security section to About dialog

Fixed

- Brought back changelog.md, will drop adding notes to version tags from now on
- Fixed race condition during import which may have led to partial imports with large imports (> 1000 rows)
- Fixed calculating max and min values to properly place the charts in the middle of the screen
- Removed "item saved" messages after an item got added, looked like an error and didn't serve a purpose because the new value is visible at the top of the list anyway.
- Enlarged the blood pressure quickedit fields somewhat to make the hint message fit in german language
- Made sure chart origins starts on a 10er number instead of uneven numbers

## v1.7.4
New

* Added quick data entry mode, on: same behaviour as before, off: open data entry form which also allows to change date and time upon entry.
* Added legends to the charts
* Added switch to turn chart legend on/off

* Fixed an issue where the app would hang during import if a filter was active
* Fixed a little glitch which mixed up date and time in dataEdit Dialogs
* Changed chart grid colour to gray to make it more subtle
* Align chart y-axis to multiples of 10
* Set chart stepping to 5 (Weight) and 10 (blood pressure)
* Fixed visual glitch in Weight Edit dialog

## 1.7.3
Fixed

- Removed the type selector for DeleteData menu. Not needed any longer 

## 1.7.2
Fixed

- Some bugfixes and code simplifcations 

## 1.7.1
Fixed

- Fixed crash in Diary module

## 1.7.0
New/improved

- Added date filter capability
- Improved usability when handling authentication errors
- Added disclaimer dialog for first launch
- Moved data import to separate thread to cater for large imports
- Large scale testing. 10k records per category = 27 years worth of one entry per day

Fixes

- Weight value was not showing when highlighting values was off
- Fixed a racing condition where clearing the db before a data import would lead to unpredictable results

## 1.6.0
App Internal - Significant refactoring 

- Switched to Google recommended architecture
- Utilizing Room, Data Migrations, Repositories, ViewModels and LiveData
- Migrated from Java to Kotlin
- Switched from fingerprint to Biometric libraries 
- Upgraded to AndroidX

New/improved

- Protect app with biometric (fingerprint) logon
- Cleaner PDF reports, no more hard to see colours but bold and underline for elevated values
- New default color theme blue, added ability to switch colour themes
- Added MovingAverage Trendlines
- Added proper edit dialogs / fragments

Fixes

- Fixed bug when highlighting elevated values, #2
- Fixed bug where deleted values remained in chart, #49
- Fixed a bug in BloodPressure PDF report. Diastolic value didn't show

## 1.5.0
Export your data before upgrading!!!
New/improved

- Major overhaul of applications inner working. Code clean up, more code reuse through class inheritance, more robust database interface
- Allow to save the Backup wherever the user has write access
- Backup as ZIP or individual files
- Added posibility to change colour theme (Green, Blue, Red)
- Added proper data edit dialogs

Fixes

- Fixed a bug in blood pressure pdf, diastolic value didn't show

## 1.3.2
New/improved
- Moved away from export/import to Backup/Restore. Backup all 3 data areas into a ZIP File. Restore can read individual files from ZIP or file system
- Allow to save the Backup wherever the user has write access

Fixes
- More error handling during file IO

## 1.3.1
New/improved
- Added ability to add name (any text really) to PDF reports via new "User name" setting
- Color code / highlight individual escalated values in Blood Pressure view, instead of the whole row
- Blood pressure PDF report now sectioned into morning /afternoon / evening to allow for quicker assessment of the situation
- Added capability to change the formating of the date/time columns via new "Date/Time" format setting. See here for syntax: https://developer.android.com/reference/java/text/SimpleDateFormat

Fixes
- More error handling during file IO
- Made sure read/write access is requested before writing to default Download folder

## Version 1.2.3
- Fixed PDFs won't open after file was created.
- Fixed crash when saving on SD-Card
- Fixed crash when creating PDF files
- Fixed crash when importing a non existing file
- Removed code signing code
- Check for empty tables before launching charts and exporting data
- Updated manifest to be able to install on SD-Card

## Version 1.2.2
- Added possibility to specify import/export directory

## Version 1.2.1
- Can't remember what I added :-)

## Version 1.2.0
- Added human readable reports
- Realigned the menu structure
- New Send menu for creating/sending reports and raw (CSV) data
- New Data Management menu for data import/export

## Version 1.0.5
- Added charts

## Version 1.0.0
- Added colour thresholds
