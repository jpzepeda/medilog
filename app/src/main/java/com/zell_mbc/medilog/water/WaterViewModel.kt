package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.*
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.base.DataViewModel
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.IOException
import java.lang.StringBuilder
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class WaterViewModel(application: Application): DataViewModel(application) {
    override val fileName = "Water.csv"
    override var itemName = app.getString(R.string.water)
    override val filterStartPref = "WATERFILTERSTART"
    override val filterEndPref = "WATERFILTEREND"

    override lateinit var repository: WaterRepository
    lateinit var dao: WaterDao
    lateinit var items: LiveData<List<Water>>
    lateinit var todaysIntake: LiveData<Int>

    override fun init() {
        super.init()

        dao = MediLogDB.getDatabase(app).waterDao()
        repository = WaterRepository(dao, filterStart, filterEnd)
        items = Transformations.switchMap(filterChanged) { repository.getItems(filterStart, filterEnd) }
        todaysIntake = getToday() // Grab todays entries
    }

    fun insert(item: Water): Long {
        var rowId = -1L
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) { rowId = repository.insert(item) }
            j.join()
        }
        return rowId
    }

    fun update(water: Water) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(water)
    }

    override fun getItem(id: Int): Water? {
        var i: Water? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            i = repository.getItem(id) }
            j.join() }
        return i
    }

    override fun getItems(order: String, filtered: Boolean): List<Water> {
        if (filtered) getFilter()
        lateinit var items: List<Water>
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            items = repository.getItems(order, filterStart, filterEnd) }
            j.join() }
        return items
    }

    // Return value summed up by day
    @SuppressLint("SimpleDateFormat")
    fun getToday():LiveData<Int> {
        val today = SimpleDateFormat("yyyyMMdd").format(Date().time)
        return repository.getDay(today)
    }

    fun getDays(): List<Water> {
        lateinit var days:List<Water>
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            days = repository.getDays() }
            j.join() }
        return days
    }

    fun getMax(): Int{
        var i = 0
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            i = repository.getMax() }
            j.join() }
        return i
    }

    fun getMin(): Int{
        var i = 0
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            i = repository.getMin() }
            j.join() }
        return i
    }

// --------

    private fun csvToItem(w: Water, csv: String): Int {
        val data = csv.split(separator!!.toRegex()).toTypedArray()

        // At a minimum 2 values need to be present, comment is optional
        if (data.size < 2) {
            return -1
        }
        try {
            val date: Date? = csvPattern.parse(data[0].trim())
            if (date != null) w.timestamp = date.time
        } catch (e: ParseException) {
            return -2
        }
        try {
            w.water = data[1].trim().toInt()
        } catch (e: NumberFormatException) {
            return -3
        }

        // Comment not always available
        w.comment = if (data.size == 3) {
            data[2]
        } else {
            ""
        }
        return 0
    }


    @SuppressLint("SimpleDateFormat")
    override suspend fun importFile(reader: BufferedReader?, replaceData: Boolean): Int {
        val tmpItems = ArrayList<Water>()
        if (reader == null) {
            return 0
        }
        try {
            val tmpLine = ""
            var showError = true
            var line: String?
            var lineNo = 0
            while (reader.readLine().also { line = it } != null) {
                // Quick line format check
                val item = Water(0, 0,"", 0, "") // Start with empty item
                // At a minimum 2 values need to be present, comment is optional
                val err = csvToItem(item, line!!)
                lineNo++
                if (err < 0) {
                    if (showError) {
                        Handler(Looper.getMainLooper()).post {
                            Toast.makeText(getApplication(), "Error $err importing line $lineNo, '$tmpLine'", Toast.LENGTH_LONG).show()
                        }
                        showError = false // Show only first error to avoid pushing hundreds of similar errors
                    }
                    continue
                }
                // Calculate day
                item.day = SimpleDateFormat("yyyyMMdd").format(item.timestamp)
                tmpItems.add(item)
            }
        }
        catch (e: IOException) {
            return -1
        }

        return transferItems(tmpItems, replaceData)
    }


    private suspend fun transferItems(tmpItems: ArrayList<Water>, replaceData: Boolean): Int {
        val records = tmpItems.size
        if (records == 0) return -2     // Delete only after a successful import

        // Delete only after a successful import
        if (replaceData) {
            deleteAll(false)
            // Wait until DB is empty
            var i = 5
            while (getSize(false) > 0 && i > 0) {
                //                   Log.d("SuspendedInsert", "Avoid race condition -> Wait 100ms")
                delay(100L)
                Log.d("--------------- Debug", "Delay:" + i--)
                if (i == 0) {
                    // If after 10 rounds the DB is not empty something odd is going on
                    Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Error! Unable to delete all old records. ", Toast.LENGTH_LONG).show() }
                }
            }
        }
        // Test this for mass imports
        for (item in tmpItems) insert(item)

        return records
    }

    // CSV header
    override fun getCsvData(filtered: Boolean): String {
        val sb = StringBuilder()

        // CSV header
        sb.append(app.getString(R.string.date) + separator + app.getString(R.string.water) + System.getProperty("line.separator"))

        // Data
        val items  = getItems("DESC", filtered)
        for (item: Water in items) {
            sb.append(csvPattern.format(item.timestamp) + separator + item.water + separator + item.comment + System.getProperty("line.separator"))
        }
        return sb.toString()
    }


    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            Toast.makeText(app, app.getString(R.string.water) + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }
        val userName = sharedPref.getString(SettingsActivity.KEY_PREF_USER, "")
        val waterUnit = sharedPref.getString(SettingsActivity.KEY_PREF_WATERUNIT, "ml")
        val highlightValues = sharedPref.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        val waterThreshold = sharedPref.getString(SettingsActivity.KEY_PREF_waterThreshold, "2000")!!.toInt()

        val document = PdfDocument()

//        int tab1 = 80;
        var pageNumber = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas
        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.isFakeBoldText = false
        pdfPaint.color = Color.BLACK

        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true

        val paintRed = Paint()
        paintRed.color = Color.RED

        // Black & White instead of colour
        val blackAndWhite = true

        // -----------

        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas
        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        var headerText = app.getString(R.string.waterReportTitle)
        if (userName != null) headerText = headerText + " " + app.getString(R.string.forString) + " " + userName

        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        i = pdfDataTop
        // Print header in bold
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.water) + " (" + waterUnit + ")", pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)
        /*
        if (true) {
            document.finishPage(page)
            return document
        }
*/
// Todo: Add comment field to pdf
        // ------------
        var left = pdfLeftBorder // Value changes based on column
        var activeColumn = 1
        val column2 = canvas.width / 2 + left

        val items = getItems("ASC", filtered)

        for (wi in items) {
            i += pdfLineSpacing
            if (i > pdfDataBottom) {
                // Second column
                if (activeColumn == 1) {
                    activeColumn = 2
                    i = pdfDataTop
                    // Column separator
                    pdfPaint.color = Color.DKGRAY
                    canvas.drawLine(column2.toFloat(), pdfHeaderBottom.toFloat(), column2.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                    pdfPaint.color = Color.BLACK
                    left = column2 + pdfLeftBorder
                    canvas.drawText(app.getString(R.string.date), left.toFloat(), i.toFloat(), pdfPaintHighlight)
                    canvas.drawText(app.getString(R.string.water) + " (" + waterUnit + ")", (left + pdfDataTab).toFloat(), i.toFloat(), pdfPaintHighlight)
                } else {
                    document.finishPage(page)
                    pageNumber += 1
                    activeColumn = 1

                    // crate a A4 page description
                    pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
                    page = document.startPage(pageInfo)
                    canvas = page.canvas
                    left = pdfLeftBorder

                    canvas.drawText(headerText, left.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                    canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                    canvas.drawLine(left.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                    canvas.drawLine(left.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                    i = pdfDataTop
                    canvas.drawText(app.getString(R.string.date), left.toFloat(), i.toFloat(), pdfPaint)
                    canvas.drawText(app.getString(R.string.water) + " (" + waterUnit + ")", (left + pdfDataTab).toFloat(), i.toFloat(), pdfPaintHighlight)
                    // ------------
                }
                i += pdfLineSpacing
            }
            canvas.drawText(toStringDate(wi.timestamp), left.toFloat(), i.toFloat(), pdfPaint)
            canvas.drawText(toStringTime(wi.timestamp), left + pdfTimeTab.toFloat(), i.toFloat(), pdfPaint)

            if (highlightValues and (wi.water > waterThreshold)) {
                if (blackAndWhite) {
                    canvas.drawText(wi.water.toString(), left + pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)
                } else { // Colour
                    canvas.drawText(wi.water.toString(), left +pdfDataTab.toFloat(), i.toFloat(), paintRed)
                }
            }
            else {
                canvas.drawText(wi.water.toString(), left + pdfDataTab.toFloat(), i.toFloat(), pdfPaint)
            }
        }
        // finish the page
        document.finishPage(page)
        return document
    }
}