package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.preference.PreferenceManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import kotlinx.android.synthetic.main.water_tab.*
import kotlinx.android.synthetic.main.watereditform.*
import kotlinx.android.synthetic.main.watereditform.btSave
import kotlinx.android.synthetic.main.watereditform.etComment
import kotlinx.android.synthetic.main.watereditform.etDate
import kotlinx.android.synthetic.main.watereditform.etTime
import kotlinx.android.synthetic.main.weighteditform.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class WaterEditFragment : Fragment() {
    private val viewModel: EditWaterViewModel by viewModels() //factoryProducer = { SavedStateViewModelFactory(this, ) })
    private val timestampCal = Calendar.getInstance()
    var _id: Int = 0

    @SuppressLint("SimpleDateFormat")
    private fun saveItem() {
        val editItem = viewModel.getItem(_id)
        if (editItem == null) {
            Snackbar.make(requireView(), "Unknown error!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Check empty variables
        val value = etEditWater.text.toString()
        if (value.isEmpty()) {
            Snackbar.make(requireView(), this.getString(R.string.waterMissing), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Valid water?
        var waterValue = 0
        try {
            waterValue = value.toInt()
            if (waterValue <= 0) {
                Snackbar.make(requireView(), this.getString(R.string.eInvalidValue1) + " " + this.getString(R.string.water) + " "+ this.getString(R.string.eInvalidValue2) + " $waterValue", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return
            }
        } catch (e: Exception) {
            Snackbar.make(requireView(), "Exception: " + this.getString(R.string.eInvalidValue1) + " " + this.getString(R.string.water) + " "+ this.getString(R.string.eInvalidValue2) + " $waterValue", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        editItem.timestamp = timestampCal.timeInMillis
        editItem.water = waterValue
        editItem.comment = etComment.text.toString()
        editItem.day = SimpleDateFormat("yyyyMMdd").format(editItem.timestamp)

        viewModel.update(editItem)

        Snackbar.make(requireView(), requireContext().getString(R.string.itemUpdated), Snackbar.LENGTH_LONG).setAction("Action", null).show()
        requireActivity().onBackPressed()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.watereditform, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val saveButton = btSave as FloatingActionButton

        when (colourStyle) {
            this.getString(R.string.green) -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> saveButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }
        saveButton.setColorFilter(Color.WHITE)

        tvEditUnit.text = sharedPref.getString(SettingsActivity.KEY_PREF_WATERUNIT, "ml")

        viewModel.init()
        val editItem = viewModel.getItem(_id)
        if (editItem == null) {
            Snackbar.make(requireView(), "Unknown error!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        this.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Check if we are really editing or if this is a new value, if new entry delete tmpComment
        if (viewModel.isTmpItem(editItem.comment))
            this.etComment.setText("")
        else {
            this.etEditWater.setText(editItem.water.toString())
            this.etComment.setText(editItem.comment)
        }

        // Respond to click events
        saveButton.setOnClickListener { saveItem() }

        // ---------------------
        // Date/Time picker section
        timestampCal.timeInMillis = editItem.timestamp

        // create an OnDateSetListener
        val dateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            timestampCal.set(Calendar.HOUR, hour)
            timestampCal.set(Calendar.MINUTE, minute)
//            val s = DateFormat.getTimeInstance().format(timestampCal.timeInMillis)
            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis) // ToDo: This call is buggy, sometimes you'll get 12 hours difference
//        Log.d("--------------- Debug", "24hour: $b")
        }

        etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    dateListener,
                    timestampCal.get(Calendar.YEAR),
                    timestampCal.get(Calendar.MONTH),
                    timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                    timeListener,
                    timestampCal.get(Calendar.HOUR_OF_DAY),
                    timestampCal.get(Calendar.MINUTE),
                    android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }

        // Make sure first field is highlighted and keyboard is open
        this.etEditWater.requestFocus()

    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.delete() // Deletes all items with comment == tmpComment
    }

    fun newInstance(i: Int): WaterEditFragment? {
        val f = WaterEditFragment()
        // Supply index input as an argument.
        f._id = i
//        Log.d("--------------- Debug", "NewInstance Index: " + _id)
        return f
    }
}

