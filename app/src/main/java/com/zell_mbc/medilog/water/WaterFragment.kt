package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import com.zell_mbc.medilog.Water
import kotlinx.android.synthetic.main.water_tab.*
import kotlinx.android.synthetic.main.water_tab.etComment
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class WaterFragment : Fragment(), WaterListAdapter.ItemClickListener {
    private lateinit var viewModel: WaterViewModel
    private var adapter: WaterListAdapter? = null
    private var quickEntry = true
    private var waterThreshold = ""

    /*    private fun tmpListener() {
            val tmp = viewModel.filterChanged.value?.plus(1L)
            viewModel.filterChanged.postValue(tmp)
            Log.d("tmpListener : ", "New value: " + viewModel.filterChanged.value)
        }
    */
    @SuppressLint("SimpleDateFormat")
    private fun addItem() {

        if (!quickEntry) {
            val tmpComment = "temporaryEntry-YouShouldNeverSeeThis"
            val tmpItem = Water(0, Date().time, tmpComment, 0)
            val _id = viewModel.insert(tmpItem)
            if (_id  != 0L) editItem(_id.toInt())
            else Snackbar.make(requireView(), "No entry with tmp water found!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Check empty variables
        val waterValue = etWater.text.toString()
        val commentValue = etComment.text.toString()
        if (waterValue.isEmpty()) {
            Snackbar.make(requireView(), getString(R.string.waterMissing), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }
        var value = 0
        try {
            value = waterValue.toInt()
            if (value <= 0) {
                Snackbar.make(requireView(), this.getString(R.string.eInvalidValue1) + " " + this.getString(R.string.water) + " " + this.getString(R.string.eInvalidValue2) + " $value", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return
            }
        } catch (e: Exception) {
            Snackbar.make(requireView(), "Exception: " + this.getString(R.string.eInvalidValue1) + " " + this.getString(R.string.water) + " " + this.getString(R.string.eInvalidValue2) + " $value", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }
        val item = Water(0, Date().time, commentValue, value, SimpleDateFormat("yyyyMMdd").format(Date().time) )

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) Snackbar.make(requireView(), getString(R.string.filteredOut), 4000).setAction("Action", null).show()

        etWater.setText("")
        etComment.setText("")
//        checkToday()
    }


    private fun editItem(index: Int) {
        val intent = Intent(requireContext(), WaterEditActivity::class.java)
        val extras = Bundle()
        val item = viewModel.getItem(index)
        if (item != null) {
            extras.putInt("ID", item._id)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun deleteItem(index: Int) {
        // Remove from array
//        Log.d("Debug waterFragment", "Delete: " + index)
        val item = viewModel.getItem(index)
        if (item != null) {
            viewModel.delete(item._id)
        }
        Snackbar.make(requireView(), viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_deleted), Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    private fun checkToday(waterToday: Int) {
        val tmpString = requireContext().getString(R.string.today) + " " +waterToday.toString() + " / $waterThreshold" + tvUnit.text
        tvTodaysIntake.text = tmpString

        if (waterToday >= waterThreshold.toInt()) tvTodaysIntake.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorWaterIntakeMet))
        else                                      tvTodaysIntake.setTextColor(tvUnit.textColors)
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.water_tab, parent, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //       Log.d("waterFragment onViewCreated : ", "Start")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(requireContext())
        quickEntry = sharedPref.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)
        val tmpString = sharedPref.getString(SettingsActivity.KEY_PREF_waterThreshold, "2000")
        if (tmpString != null) waterThreshold = tmpString
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val addButton = btAdd as FloatingActionButton
//        val tmpButton = btTmp as FloatingActionButton

        when (colourStyle) {
            this.getString(R.string.green) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }
        addButton.setColorFilter(Color.WHITE)

        if (!quickEntry) { // Hide quick entry fields
            etWater.visibility = View.GONE
            etComment.visibility = View.GONE
            tvUnit.visibility = View.GONE
        }
        else {
            etWater.visibility = View.VISIBLE
            etComment.visibility = View.VISIBLE
            tvUnit.visibility = View.VISIBLE
            tvUnit.text = sharedPref.getString(SettingsActivity.KEY_PREF_WATERUNIT, "ml")
        }

        val layoutManager = LinearLayoutManager(requireContext())
        rvWaterList.layoutManager = layoutManager

        adapter = WaterListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(WaterViewModel::class.java)
        viewModel.items.observe(requireActivity(), { water -> water?.let { adapter!!.setItems(it) } })

        val observer = Observer<Int> {
            if (it != null) checkToday(it)
            else checkToday(0)
            } // Looks like "it" is null when no value exists for today
        viewModel.todaysIntake.observe(requireActivity(), observer) //Observer { checkToday(it) })

        adapter!!.setClickListener(this)
        rvWaterList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(rvWaterList.context, layoutManager.orientation)
        rvWaterList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        addButton.setOnClickListener { addItem() }

        val showChartButton = btShowWaterChart as FloatingActionButton
        when (colourStyle) {
            this.getString(R.string.green) -> showChartButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGreen))
            this.getString(R.string.blue) -> showChartButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightBlue))
            this.getString(R.string.red) -> showChartButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightRed))
        }

        showChartButton.setOnClickListener(View.OnClickListener {
            if (viewModel.getSize(true) < 2) {
                Snackbar.make(view, requireContext().getString(R.string.notEnoughDataForChart), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                //               Toast.makeText(c, c.getString(R.string.notEnoughDataForChart), Toast.LENGTH_LONG).show()
                return@OnClickListener
            }

            val intent = Intent(requireContext(), WaterChartActivity::class.java)
            startActivity(intent)
        })
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        MaterialDialog(requireContext()).show {
            title(text = getString(R.string.ItemClicked) + " " + dateFormat.format(item.timestamp))
            message(R.string.whatDoYouWant)
            neutralButton(R.string.cancel)
            positiveButton(R.string.delete) { deleteItem(item._id) }
            negativeButton(R.string.edit) { editItem(item._id) }
        }
    }

}