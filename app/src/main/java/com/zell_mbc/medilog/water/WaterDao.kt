package com.zell_mbc.medilog.water

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.PrimaryKey
import androidx.room.Query
import com.zell_mbc.medilog.Water
import com.zell_mbc.medilog.base.DataDao
import com.zell_mbc.medilog.water.DaySum

@Dao
abstract class WaterDao: DataDao<Water> {

/*    @Query("SELECT SUM(water) from water where day = :day group by day")
    abstract suspend fun getDay(day: String): Int
*/
    //-------------------
    @Query("SELECT _id, timestamp, comment, SUM(water) as water, day from water group by day")
    abstract fun getDays(): List<Water>

    @Query("SELECT _id, timestamp, comment, SUM(water) as water, day from water WHERE timestamp >=:start group by day")
    abstract fun getDaysGreaterThan(start: Long): List<Water>

    @Query("SELECT _id, timestamp, comment, SUM(water) as water, day from water WHERE timestamp <=:end group by day")
    abstract fun getDaysLessThan(end: Long): List<Water>

    @Query("SELECT _id, timestamp, comment, SUM(water) as water, day from water WHERE timestamp >=:start and timestamp <=:end group by day")
    abstract fun getDaysRange(start: Long, end: Long): List<Water>
    //-------------------

//    @Query("SELECT _id, timestamp, comment, SUM(water) as water, day from water WHERE day = :day")
    @Query("SELECT SUM(water) from water WHERE day = :day")
    abstract fun getDay(day: String): LiveData<Int>

    @Query("DELETE from water where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from water where comment = :tmpComment")
    abstract suspend fun delete(tmpComment: String)

    @Query("SELECT MIN(water) FROM water")
    abstract fun getMinWater(): Int

    @Query("SELECT MAX(water) FROM water")
    abstract fun getMaxWater(): Int

    // ----------------------------
    @Query("SELECT COUNT(*) FROM water")
    abstract fun getSize(): Int

    @Query("SELECT COUNT(*) FROM water WHERE timestamp >=:start")
    abstract fun getSizeGreaterThan(start: Long): Int

    @Query("SELECT COUNT(*) FROM water WHERE timestamp <=:end")
    abstract fun getSizeLessThan(end: Long): Int

    @Query("SELECT COUNT(*) FROM water WHERE timestamp >=:start and timestamp <=:end")
    abstract fun getSizeRange(start: Long, end: Long): Int
    // ----------------------------

    @Query("SELECT * from water where _id = :id")
    abstract fun getItem(id: Int): Water

    // ----------------------------
    // getWaterItems options, return type List
    // ------------ASC ------------
    @Query("SELECT * from water ORDER BY timestamp ASC") // Used by Chart fragment
    abstract fun getWaterItemsASC(): List<Water>

    @Query("SELECT * from water WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getWaterItemsASCRange(start: Long, end: Long): List<Water>

    @Query("SELECT * from water WHERE timestamp >=:start ORDER BY timestamp ASC")
    abstract fun getWaterItemsASCGreaterThan(start: Long): List<Water>

    @Query("SELECT * from water WHERE timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getWaterItemsASCLessThan(end: Long): List<Water>

    // ------------DESC ------------
    @Query("SELECT * from water ORDER BY timestamp DESC")
    abstract fun getWaterItemsDESC(): List<Water>

    @Query("SELECT * from water WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getWaterItemsDESCRange(start: Long, end: Long): List<Water>

    @Query("SELECT * from water WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getWaterItemsDESCGreaterThan(start: Long): List<Water>

    @Query("SELECT * from water WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getWaterItemsDESCLessThan(end: Long): List<Water>

    // ----------------------------
    // getWaterItems options, return type LiveData
    // Used by Recycler view
    @Query("SELECT * from water WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsRange(start: Long, end: Long): LiveData<List<Water>>

    @Query("SELECT * from water WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsGreaterThan(start: Long): LiveData<List<Water>>

    @Query("SELECT * from water WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsLessThan(end: Long): LiveData<List<Water>>

    @Query("SELECT * from water ORDER BY timestamp DESC")
    abstract fun getItems(): LiveData<List<Water>>

    // ----------------------

    @Query("DELETE FROM water")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM water WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)

}
