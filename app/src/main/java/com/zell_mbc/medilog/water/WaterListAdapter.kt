package com.zell_mbc.medilog.water

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import com.zell_mbc.medilog.Water
import java.text.DateFormat

class WaterListAdapter internal constructor(context: Context) : RecyclerView.Adapter<WaterListAdapter.WaterViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var clickListener: ItemClickListener? = null

    private var items = emptyList<Water>() // Cached copy of waters, no ide why I need it

    private val textSize: Float
    private var waterUnit = ""
    private var dateFormat: DateFormat
    private var timeFormat: DateFormat
    private val highlightValues: Boolean
    private val waterThreshold: Int

    inner class WaterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val dateItem: TextView = itemView.findViewById(R.id.tvDateItem)
        val waterItem: TextView = itemView.findViewById(R.id.tvWaterItem)
        val commentItem: TextView = itemView.findViewById(R.id.tvCommentItem)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            dateItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            waterItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            commentItem.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WaterViewHolder {
        val itemView = inflater.inflate(R.layout.waterview_row, parent, false)
        return WaterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WaterViewHolder, position: Int) {
        val current = items[position]
        val tmpString = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
        holder.dateItem.text = tmpString

        val i = current.water
        val s = i.toString() + waterUnit
        if (highlightValues) {
                holder.waterItem.text = s
       }
        else holder.waterItem.text = s

        holder.commentItem.text = current.comment
    }

    internal fun setItems(items: List<Water>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun getItemAt(position: Int): Water {
        return items[position]
    }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        waterUnit = " " + sharedPref.getString(SettingsActivity.KEY_PREF_WATERUNIT, "ml")
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)

        textSize = java.lang.Float.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, "15")!!)
        highlightValues = sharedPref.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        waterThreshold = Integer.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_waterThreshold, "2000")!!)
    }
}
