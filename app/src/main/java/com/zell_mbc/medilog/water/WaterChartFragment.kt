package com.zell_mbc.medilog.water

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import java.text.*
import java.util.*
import kotlin.math.max
import kotlin.math.roundToInt

// Chart manual
// https://github.com/halfhp/androidplot/blob/master/docs/xyplot.md
class WaterChartFragment : Fragment() {
    private var waterTreshold = ArrayList<Int>()
    private var waters = ArrayList<Int>()
    private var linearTrend = ArrayList<Int>()
    private var movingAverage = ArrayList<Int>()
    private var period = 5 // Minimum value = 2


    private fun calculateMovingAverage() {
        val sample = Array(size = period, init = { 0}) // Create array of float, with all values set to 0
        val n = waters.size
        var sma: Int

        // the first n values in the sma will be off -> set them to the first water value
        for (i in 0 until period) {
            sample[i] = waters[0]
        }

        for (i in 0 until n) {
            for (ii in 0..period-2) {
                sample[ii] = sample[ii + 1]
            }
            sample[period-1] = waters[i]

            sma = 0
            for (ii: Int in 0 until period) {
                sma += sample[ii]
            }
            sma /= period
            movingAverage.add(sma)
        }
    }

    private fun calculateLinearTrendLine() {
        // https://classroom.synonym.com/calculate-trendline-2709.html
        var a = 0
        val b: Int
        var b1 = 0
        var b2 = 0
        var c = 0
        val d: Int
        val e: Int
        val f: Int
        val g: Int
        val m: Int
        val n = waters.size
        for (i in 1..n) {
            a += i * waters[i - 1]
            b1 += i
            b2 += waters[i - 1]
            c += i * i
        }
        a *= n
        b = b1 * b2
        c *= n
        d = b1 * b1
        m = ((a - b) / (c - d))
        e = b2
        f = m * b1
        g = (e - f) / n
        var value: Int
        for (i in 1..n) {
            value = m * i + g
            linearTrend.add(value)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.water_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val c = context
        if (c == null) {
            Log.d("--------------- Debug", "Empty Context")
            return
        }

        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        var wMax = 0
        var wMin = 10000

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(c)

        val daySteppingMode = sharedPref.getBoolean(SettingsActivity.KEY_PREF_waterDayStepping, false)
        val barChart = sharedPref.getBoolean(SettingsActivity.KEY_PREF_waterBarChart, true)
        var sTmp: String

        var fTmp: Int
        val simpleDate = SimpleDateFormat("MM-dd")
        val lastDate = Calendar.getInstance()
        var currentDate: Date?

        val viewModel = ViewModelProvider(this).get(WaterViewModel::class.java)
        viewModel.init()
        val items = viewModel.getDays()
        for (wi in items) {
            sTmp = simpleDate.format(wi.timestamp)

            // Chart stepping by day
            if (daySteppingMode) {
                // Fill gap
                currentDate = try {
                    simpleDate.parse(sTmp)
                } catch (e: ParseException) {
                    continue
                }

                /*currentDate.
                Log.d("--------------- Debug", "currentDate: $currentDate")
                Log.d("--------------- Debug", "lastDate: " + lastDate.time)
                Log.d("--------------- Debug", "Compare: " + currentDate.compareTo(lastDate.time)) */
                if (currentDate != null) {
                    while (labels.size > 0 && currentDate > lastDate.time) {
                        sTmp = simpleDate.format(lastDate.time)
                        labels.add(sTmp)
                        Log.d("--------------- Debug", "GapDate: $sTmp")
                        waters.add(0)
                        lastDate.add(Calendar.DAY_OF_MONTH, 1)
                    }
                }
                if (currentDate != null) lastDate.time = currentDate
            }
            labels.add(sTmp)
            waters.add(wi.water)

            // Keep min and max values
            fTmp = wi.water
            if (fTmp > wMax) {
                wMax = fTmp
            }
            if (fTmp < wMin) {
                wMin = fTmp
            }
        }
        if (waters.size == 0) {
            return
        }

        // If threshold is set create dedicated chart, otherwise show as origin
        val treshold = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWTHRESHOLDS, true)
        val s = sharedPref.getString(SettingsActivity.KEY_PREF_waterThreshold, "2000")
        val waterThreshold = Integer.valueOf(s!!)
        if (treshold) {
            for (item in waters) {
                waterTreshold.add(waterThreshold)
            }
        }

        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.waterPlot)
        val showGrid = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWGRID, true)
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = Paint(Color.GRAY)
            plot.graph.rangeGridLinePaint = Paint(Color.GRAY) // Horizontal lines
        }

        val isLegendVisible = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWLEGEND, true)
        plot.legend.isVisible = isLegendVisible

        val series1: XYSeries = SimpleXYSeries(waters, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.water))
        val series2: XYSeries = SimpleXYSeries(waterTreshold, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.threshold))

        // Set boundaries to 10th at -20 and + 20 of min and max value
        val wMinBoundary = 0 // wMin - 10
        val wMaxBoundary = max(wMax,waterThreshold)  + 10
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary/10*10) // Set to a 10er value

/*        Log.d("--------------- Debug", "wMin: $wMin")
        Log.d("--------------- Debug", "wMax: $wMax")
        Log.d("--------------- Debug", "wMinBoundary: $wMinBoundary")
        Log.d("--------------- Debug", "wMaxBoundary: $wMaxBoundary")
*/

        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 100.0)
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("####") // + waterUnit));  // Set integer y-Axis label

        // Line charts don't work without additional effort with date gaps, hence we switch to bar charts
        // Bar chart
        if (barChart) {
            val series1Format = BarFormatter(Color.BLUE, Color.BLUE)
            plot.addSeries(series1, series1Format)
        } else {
            val series1Format = LineAndPointFormatter(Color.BLUE, null, null, null)
            if (treshold) {
                val series1FormatLight = LineAndPointFormatter(Color.parseColor("#ADD8E6"), null, null, null)
                plot.addSeries(series2, series1FormatLight)
            }
            plot.addSeries(series1, series1Format)
        }
/*
        // Trendlines
        val waterLinarTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_WATER_LINEAR_TRENDLINE, false)
        val waterMovingAverageTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_WATER_MOVING_AVERAGE_TRENDLINE, false)

        if (waterLinarTrendLine) {
            calculateLinearTrendLine()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            val trendLine: XYSeries = SimpleXYSeries(linearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Linear Trend")

            trendFormat.isLegendIconEnabled = false
            plot.addSeries(trendLine, trendFormat)
        }

        if (waterMovingAverageTrendLine) {
            period = sharedPref.getString(SettingsActivity.KEY_PREF_WATER_MOVING_AVERAGE_SIZE, "5")!!.toInt()
            calculateMovingAverage()

            val trendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            trendFormat.isLegendIconEnabled = false
            val simpleMovingAverage: XYSeries = SimpleXYSeries(movingAverage, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Moving Average")
            plot.addSeries(simpleMovingAverage, trendFormat)
        }
*/
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}