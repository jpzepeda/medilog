package com.zell_mbc.medilog.water

import android.util.Log
import androidx.lifecycle.LiveData
import com.zell_mbc.medilog.base.DataRepository
import com.zell_mbc.medilog.Water

class WaterRepository(private val dao: WaterDao, fS:Long, fE: Long): DataRepository() {
    init {
        filterStart = fS
        filterEnd = fE
    }

    fun getItems(filterStart: Long, filterEnd: Long): LiveData<List<Water>> {
        return if ((filterStart != 0L) && (filterEnd == 0L)) dao.getItemsGreaterThan(filterStart)
        else if ((filterStart == 0L) && (filterEnd != 0L)) dao.getItemsLessThan(filterEnd)
        else if ((filterStart != 0L) && (filterEnd != 0L)) dao.getItemsRange(filterStart, filterEnd)
        else dao.getItems()
    }

    override fun getItems(order: String, filterStart: Long, filterEnd: Long): List<Water> {
        if (order == "ASC") {
            if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getWaterItemsASCGreaterThan(filterStart)
            if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getWaterItemsASCLessThan(filterEnd)
            if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getWaterItemsASCRange(filterStart, filterEnd)
            return dao.getWaterItemsASC()
        } else {
            if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getWaterItemsDESCGreaterThan(filterStart)
            if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getWaterItemsDESCLessThan(filterEnd)
            if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getWaterItemsDESCRange(filterStart, filterEnd)
            return dao.getWaterItemsDESC()
        }
    }

    override fun getSize(filterStart: Long, filterEnd: Long): Int {
        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getSizeGreaterThan(filterStart)
        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getSizeLessThan(filterEnd)
        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getSizeRange(filterStart, filterEnd)
        return dao.getSize()
    }

    override fun getItem(index: Int): Water = dao.getItem(index)

    fun getMin(): Int = dao.getMinWater()
    fun getMax(): Int = dao.getMaxWater()

    suspend fun insert(item: Water): Long { return dao.insert(item) }
    suspend fun update(item: Water) { dao.update(item) }
    suspend fun delete(item: Water) { dao.delete(item) }

    // Return values summed up by day
    fun getDay(day: String):LiveData<Int> {
        val ret = dao.getDay(day)
        return ret
    }
    fun getDays():List<Water> {
        return if ((filterStart != 0L) && (filterEnd == 0L)) dao.getDaysGreaterThan(filterStart)
        else if ((filterStart == 0L) && (filterEnd != 0L)) dao.getDaysLessThan(filterEnd)
        else if ((filterStart != 0L) && (filterEnd != 0L)) dao.getDaysRange(filterStart, filterEnd)
        else dao.getDays()
    }

    override suspend fun delete(tmpComment: String) {
        dao.delete(tmpComment)
    }

    override suspend fun delete(id: Int) {
        dao.delete(id)
    }

    override suspend fun deleteAll() {
        dao.deleteAll()
    }

    override suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) {
        dao.deleteAllFiltered(filterStart, filterEnd)
    }

}