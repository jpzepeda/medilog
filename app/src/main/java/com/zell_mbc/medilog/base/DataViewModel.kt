package com.zell_mbc.medilog.base

import android.annotation.SuppressLint
import android.app.Application
import android.content.ContentValues
import android.content.SharedPreferences
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.DataItem
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity.Companion.KEY_PREF_DELIMITER
import com.zell_mbc.medilog.Water
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

abstract class DataViewModel(application: Application): AndroidViewModel(application) {
    val app = application

    abstract val fileName: String
    abstract var itemName: String

    abstract val filterStartPref: String
    abstract val filterEndPref: String
    var filterStart = 0L
    var filterEnd = 0L
    var filterChanged = MutableLiveData<Long>(0L)

    // PDF margin defaults
    val pdfHeaderTop = 30
    val pdfHeaderBottom = 50
    val pdfDataTop = 70
    val pdfLineSpacing = 15
    val pdfLeftBorder = 10
    val pdfTimeTab = 80 // Left offset for time
    val pdfDataTab = 150

    var pdfRightBorder = 0 // Set in fragment based on canvas size - pdfLeftBorder
    var pdfDataBottom = 0 // Set in fragment based on canvas size

    abstract val repository: DataRepository

    @JvmField
    val sharedPref: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    @JvmField
    val separator = sharedPref.getString(KEY_PREF_DELIMITER, ",")
    @SuppressLint("SimpleDateFormat")
    var csvPattern = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    abstract fun createPdfDocument(filtered: Boolean): PdfDocument?
    abstract fun getCsvData(filtered: Boolean): String
    abstract suspend fun importFile(reader: BufferedReader?, replaceData: Boolean): Int

    // Constructor
    open fun init() {
        getFilter()
    }

    fun toStringDate(l: Long): String {
        return DateFormat.getDateInstance().format(l)
    }

    @SuppressLint("SimpleDateFormat")
    fun toStringTime(l: Long): String {
        return SimpleDateFormat("HH:mm").format(l)
    }

    fun delete(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(id)
    }

    // Resets the filter an deletes all rows
    fun deleteAll(filtered: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        if (filtered) {
            // If no end date specified set fake one to avoid a '...where timestamp <:0' query
            var tmpFilter = filterEnd
            if (tmpFilter == 0L) tmpFilter = Calendar.getInstance().timeInMillis + (86400000 * 10) // Add a 10 days worth of milliseconds
            repository.deleteAllFiltered(filterStart, tmpFilter)
        }
        else repository.deleteAll()
    }

    abstract fun getItem(id: Int): DataItem?
    abstract fun getItems(order: String, filtered: Boolean): List<DataItem>

    fun getSize(filtered: Boolean): Int {
        if (filtered) getFilter()
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = if (filtered) repository.getSize(filterStart, filterEnd)
                else repository.getSize(0L, 0L)
            }
            j.join()
        }
        return i
    }

    fun getFilter() {
        filterStart = sharedPref.getLong(filterStartPref, 0L)
        filterEnd = sharedPref.getLong(filterEndPref, 0L)
    }

    fun setFilter(start: Long, end: Long) {
        val editor = sharedPref.edit()
        editor.putLong(filterStartPref, start)
        editor.putLong(filterEndPref, end)
        editor.apply()

        filterStart = start
        filterEnd = end
        filterChanged.value = (filterStart + filterEnd) // Trigger data refresh
//        Log.d("DataViewModel : ", "New value: " + filterChanged.value)
    }

    fun resetFilter() {
        val editor = sharedPref.edit()
        editor.putLong(filterStartPref, 0L)
        editor.putLong(filterEndPref, 0L)
        editor.apply()
        filterStart = 0L
        filterEnd = 0L
        filterChanged.value = 0L
        Log.d("DataViewModel : ", "New value: " + filterChanged.value)
    }

    fun setTodayFilter() {
        // Set "today" filter
        val cal: Calendar = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        setFilter(cal.timeInMillis, 0)
    }


    fun suspendedFileImport(reader: BufferedReader?, replaceData: Boolean): Int {
        var i = 0
        runBlocking {
            val j = viewModelScope.launch(Dispatchers.IO) {
                i = importFile(reader, replaceData)
            }
            j.join()
        }

        when (i) {
            -1 -> {
                Toast.makeText(app, app.getString(R.string.eReadError), Toast.LENGTH_SHORT).show()
            } // IO Exception
            -2 -> {
                Toast.makeText(app, app.getString(R.string.eValueFormat) + ", no Data imported", Toast.LENGTH_SHORT).show()
            } // No lines to import
            else -> {
                resetFilter()
                Toast.makeText(app, i.toString() + " " + itemName + " " + app.getString(R.string.recordsLoaded), Toast.LENGTH_SHORT).show()
            }
        }
        return i
    }


    fun getCsvFile(filtered: Boolean): Uri? {
        val filePath = File(app.cacheDir, "")
        val newFile = File(filePath, fileName)
        val uri = FileProvider.getUriForFile(app, app.applicationContext.packageName + ".provider", newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
//                val bArray = csvData(filtered = true).toByteArray(Charsets.UTF_8)
                out.write(getCsvData(filtered).toByteArray(Charsets.UTF_8))
                out.flush()
                out.close()
            }
        } catch (e: IOException) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }
        return uri
    }

    fun getPdfFile(document: PdfDocument?): Uri? {
        if (document == null) {
            return null
        }
//        val fn = fileName!!.replace("csv", "pdf")
        val filePath = File(app.cacheDir, "")
        val newFile = File(filePath, fileName.replace("csv", "pdf"))
        val uri = FileProvider.getUriForFile(app, app.applicationContext.packageName + ".provider", newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.writeTo(out)
                out.flush()
                out.close()
            }
        } catch (e: IOException) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }
        return uri
    }

    @SuppressLint("SimpleDateFormat")
    fun exportCSV(backupFolderUri: Uri?, csv: String) {
        var dFile: DocumentFile? = null
        try {
            val dFolder = DocumentFile.fromTreeUri(app, backupFolderUri!!)
            //             Log.e("Debug", "isDirectory: " + dFolder.isDirectory());
            val fn = fileName.replace(".", "-" + SimpleDateFormat("yyyy-MM-dd").format(Date().time) + ".")
            if (dFolder != null) dFile = dFolder.createFile("application/csv", fn)
            if (dFile == null) {
                Toast.makeText(app, app.getString(R.string.eCreateFile) + ": " + dFile, Toast.LENGTH_LONG).show()
                return
            }
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + ": " + dFile + " " + e.toString(), Toast.LENGTH_LONG).show()
            return
        }
        val o: OutputStream?
        o = try {
            app.contentResolver.openOutputStream(dFile.uri)
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return
        }
        if (o == null) return
        try {
            o.write(csv.toByteArray())
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eWriteError) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return
        }
        try {
            o.close()
        } catch (e: Exception) {
            Toast.makeText(app, app.getString(R.string.eCloseFile) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return
        }

        Toast.makeText(app, app.getString(R.string.fileCreated) + " " + dFile.name + "\n" + csv.lines().count() + " " + app.getString(R.string.itemsExported), Toast.LENGTH_LONG).show()
    }

    @SuppressLint("SimpleDateFormat")
    fun getZIP(document: PdfDocument?, zipPassword: String): Uri? {
        if (document == null) {
            Toast.makeText(app, fileName.replace(".csv", "") + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        // https://github.com/srikanth-lingala/zip4j

        // Create temporary file
        var fn = fileName.replace("csv", "pdf")
        var filePath = File(app.externalCacheDir, "")
        val newFile = File(filePath, fn)
        var uri = FileProvider.getUriForFile(app, app.applicationContext.packageName + ".provider", newFile)
        try {
            val out = app.contentResolver.openOutputStream(uri)
            if (out != null) {
                document.writeTo(out)
                out.flush()
                out.close()
            }
        } catch (e: IOException) {
            Toast.makeText(app, app.getString(R.string.eCreateFile) + " " + newFile, Toast.LENGTH_LONG).show()
            return null
        }

        // Create ZIP File
        // Prepare ZIP file
        val zipParameters = ZipParameters()
        zipParameters.fileNameInZip = newFile.toString()
        zipParameters.fileNameInZip = fn
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        val zipFileName = "MediLog-" + fileName.replace(".csv", "") + "-" + SimpleDateFormat("yyyy-MM-dd").format(Date().time) + ".zip"

        if (Build.VERSION.SDK_INT < 29) {
            filePath = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "")
            fn = "$filePath/$zipFileName"
            // ---------------

            val zf: ZipFile = if (zipPassword.isEmpty()) {
                ZipFile(fn)
            } else {
                ZipFile(fn, zipPassword.toCharArray())
            }
            try {
                zf.addFile(newFile.toString(), zipParameters)
            } catch (e: IOException) {
                Toast.makeText(app, app.getString(R.string.eShareError) + " " + zf, Toast.LENGTH_LONG).show()
                return null
            }

            // Delete tmpFile
            newFile.delete()
            val fileWithinMyDir = File(fn)
            if (fileWithinMyDir.exists()) {
                uri = FileProvider.getUriForFile(app, app.applicationContext.packageName + ".provider", fileWithinMyDir)
                var protected = " "
                if (zipPassword.isNotEmpty()) protected = " " + app.getString(R.string.protect) + " "
                Toast.makeText(app, app.getString(R.string.fileCreated) + protected + zipFileName, Toast.LENGTH_LONG).show()
                return uri
            }
            return null
        }
        // ------ Replacement for deprecated Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) call
        else { // API level >= 29
            // Get Uri for new ZIP file
            val resolver = app.contentResolver
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, zipFileName)
                put(MediaStore.MediaColumns.MIME_TYPE, "application/zip")
                put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS)
            }

            val zipFileUri = resolver.insert(MediaStore.Files.getContentUri("external"), contentValues)

            val zout: ZipOutputStream
            try {
                zout = if (zipPassword.isNotEmpty()) {
                    ZipOutputStream(resolver.openOutputStream(zipFileUri!!), zipPassword.toCharArray())
                } else ZipOutputStream(resolver.openOutputStream(zipFileUri!!))

                zout.putNextEntry(zipParameters)
                zout.closeEntry()
                zout.close()
                var protected = " "
                if (zipPassword.isNotEmpty()) protected = " " + app.getString(R.string.protect) + " "
                Toast.makeText(app, app.getString(R.string.fileCreated) + protected + zipFileName, Toast.LENGTH_LONG).show()
                return zipFileUri
            } catch (e: IOException) {
                Toast.makeText(app, app.getString(R.string.eShareError) + " " + zipFileName, Toast.LENGTH_LONG).show()
                return null
            }
        }
    }
}