package com.zell_mbc.medilog

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.base.DataViewModel
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.water.WaterViewModel
import com.zell_mbc.medilog.weight.WeightViewModel
import kotlinx.android.synthetic.main.filter.*
import java.text.DateFormat
import java.util.*


class FilterFragment: DialogFragment() {
    var filterStart = 0L
    var filterEnd = 0L
    private val filterStartCal: Calendar = Calendar.getInstance()
    private val filterEndCal: Calendar = Calendar.getInstance()  // Sets end and start dates to today

    lateinit var viewModel: DataViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.filter, container, false)
    }


    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var activeTab = ""
        if (arguments != null) activeTab = arguments?.getString("activeTab").toString()
        if (activeTab == "") return super.onViewCreated(view, savedInstanceState)

        when (activeTab) {
            getString(R.string.tab_weight) -> viewModel = ViewModelProvider(requireActivity()).get(WeightViewModel::class.java)
            getString(R.string.tab_bloodpressure) -> viewModel = ViewModelProvider(requireActivity()).get(BloodPressureViewModel::class.java)
            getString(R.string.tab_diary) -> viewModel = ViewModelProvider(requireActivity()).get(DiaryViewModel::class.java)
            getString(R.string.tab_water) -> viewModel = ViewModelProvider(requireActivity()).get(WaterViewModel::class.java)
        }

        filterStart = viewModel.filterStart
        filterEnd = viewModel.filterEnd

        tvHeader.text = getString(R.string.setFilter)

        filterStartCal.timeInMillis = filterStart
        if (filterEnd != 0L) filterEndCal.timeInMillis = filterEnd

        // Set button text to date if a filter is set
        if (filterStart != 0L) btStartDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterStart)

        // Set button text to date if a filter is set
        if (filterEnd != 0L) btEndDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterEnd) // Set to today


        // create an OnDateSetListener
        val startDateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            filterStartCal.set(Calendar.YEAR, year)
            filterStartCal.set(Calendar.MONTH, monthOfYear)
            filterStartCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            if (filterStartCal < filterEndCal) {
                btStartDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterStartCal.time)  // Update the text for the Start-Button
                filterStart = filterStartCal.timeInMillis
            } else {
                Snackbar.make(requireView(), getString(R.string.invalidFilterStartDate), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                filterStartCal.timeInMillis = viewModel.getItems("ASC", filtered = false)[0].timestamp // Reset to timestamp of first DB entry
            }
        }

        val endDateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            filterEndCal.set(Calendar.YEAR, year)
            filterEndCal.set(Calendar.MONTH, monthOfYear)
            filterEndCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            if (filterEndCal > filterStartCal) {
                btEndDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(filterEndCal.time)
                filterEnd = filterEndCal.timeInMillis
            } else {
                Snackbar.make(requireView(), getString(R.string.invalidFilterEndDate), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            }
        }

        //##############
        btCancel.setOnClickListener {
            dismiss()
        }

        btSave.setOnClickListener {
            viewModel.setFilter(filterStart, filterEnd) // Make sure Values are updated
            requireActivity().invalidateOptionsMenu()  // Make sure the filter icon is up to date

            dismiss()
        }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        btStartDate.setOnClickListener {
            if (filterStartCal.timeInMillis == 0L) {
                val items = viewModel.getItems("ASC", filtered = false)
                if (items.count() > 0) filterStartCal.timeInMillis = items[0].timestamp
                else  {
                    Snackbar.make(requireView(), getString(R.string.emptyTable), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                    return@setOnClickListener
                }
            }
            DatePickerDialog(requireContext(),
                    startDateListener,
                    // set DatePickerDialog to point to the start of DB or today's date when it loads up
                    filterStartCal.get(Calendar.YEAR),
                    filterStartCal.get(Calendar.MONTH),
                    filterStartCal.get(Calendar.DAY_OF_MONTH)).show()
            filterStart = filterStartCal.timeInMillis
        }

        btEndDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    endDateListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    filterEndCal.get(Calendar.YEAR),
                    filterEndCal.get(Calendar.MONTH),
                    filterEndCal.get(Calendar.DAY_OF_MONTH)).show()
            filterEnd = filterEndCal.timeInMillis
        }

        btDeleteFilterStart.setOnClickListener {
            btStartDate.text = getString(R.string.startDate)
            filterStart = 0
            filterStartCal.timeInMillis = 0
        }


        btDeleteFilterEnd.setOnClickListener {
            btEndDate.text = getString(R.string.endDate)
            filterEndCal.timeInMillis = Calendar.getInstance().timeInMillis
            filterEnd = 0
        }

    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}