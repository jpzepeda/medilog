package com.zell_mbc.medilog

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.bloodpressure.BloodPressureHelper


class SettingsActivity : AppCompatActivity(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

/*    <Preference
    app:fragment="com.zell_mbc.medilog.WaterSettingsFragment"
    android:title="Water settings" />


    @SuppressLint("ResourceType")
    */

        override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
            // Instantiate the new Fragment
/*            val args = pref.extras
            val fragment = supportFragmentManager.fragmentFactory.instantiate(
                    classLoader,
                    pref.fragment)
            fragment.arguments = args
            fragment.setTargetFragment(caller, 0)
            // Replace the existing Fragment with the new Fragment
            supportFragmentManager.beginTransaction()
                    .replace(R.xml.preferences, fragment)
                    .addToBackStack(null)
                    .commit()*/
            return true
        }

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        when (sharedPref.getString(KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))) {
            this.getString(R.string.green) -> theme.applyStyle(R.style.AppThemeGreenBar, true)
            this.getString(R.string.blue)  -> theme.applyStyle(R.style.AppThemeBlueBar, true)
            this.getString(R.string.red)   -> theme.applyStyle(R.style.AppThemeRedBar, true)
        }
        supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
    }

    public override fun onStop() {
        super.onStop()
        // Check validity of bp values
        BloodPressureHelper(this)
    }

    companion object {
        const val KEY_PREF_DELIMITER = "delimiter"

        const val KEY_PREF_WEIGHTUNIT = "weightUnit"
        const val KEY_PREF_showWeightTab = "showWeightTab"
        const val KEY_PREF_weightThreshold = "weightThreshold"

        const val KEY_PREF_showWaterTab = "cbShowWaterTab"
        const val KEY_PREF_waterThreshold = "evWaterThreshold"
        const val KEY_PREF_WATERUNIT = "evWaterUnit"
        const val KEY_PREF_WATER_LINEAR_TRENDLINE = "cb_waterLinearTrendline"
        const val KEY_PREF_WATER_MOVING_AVERAGE_TRENDLINE = "cb_waterMovingAverageTrendline"
        const val KEY_PREF_WATER_MOVING_AVERAGE_SIZE = "et_waterMovingAverageSize"
        const val KEY_PREF_waterDayStepping = "checkBox_waterDayStepping"
        const val KEY_PREF_waterBarChart = "cbWaterBarChart"

        const val KEY_PREF_COLOUR = "checkBox_setColour"
        const val KEY_PREF_showBloodPressureTab = "showBloodPressureTab"
        const val KEY_PREF_showDiaryTab = "showDiaryTab"
        const val KEY_PREF_TEXT_SIZE = "listTextSize"
        const val KEY_PREF_USER = "userName"
        const val KEY_PREF_hyperGrade3 = "grade3"
        const val KEY_PREF_hyperGrade2 = "grade2"
        const val KEY_PREF_hyperGrade1 = "grade1"
        const val KEY_PREF_WEIGHT_LINEAR_TRENDLINE = "cb_weightLinearTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE = "cb_weightMovingAverageTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE = "et_weightMovingAverageSize"
        const val KEY_PREF_BLOODPRESSURE_LINEAR_TRENDLINE = "cb_bloodPressureLinearTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_TRENDLINE = "cb_bloodPressureMovingAverageTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE = "et_bloodPressureMovingAverageSize"
        const val KEY_PREF_weightDayStepping = "checkBox_weightDayStepping"
        const val KEY_PREF_bpDayStepping = "checkBox_bpDayStepping"
        const val KEY_PREF_weightBarChart = "checkBox_weightBarChart"
        const val KEY_PREF_bpBarChart = "checkBox_bpBarChart"
        const val KEY_PREF_ZIPBACKUP = "zipBackup"
        const val KEY_PREF_COLOUR_STYLE = "colourStyle"
        const val KEY_PREF_PASSWORD = "zipPassword"
        const val KEY_PREF_BIOMETRIC = "enableBiometric"
        const val KEY_PREF_SHOWTHRESHOLDS = "checkBox_showThresholds"
        const val KEY_PREF_SHOWGRID = "checkBox_showGrid"
        const val KEY_PREF_SHOWLEGEND = "checkBox_showLegend"
        const val KEY_PREF_SHOWPULSE = "cb_showPulse"
        const val KEY_PREF_QUICKENTRY = "quickEntry"
    }
}