package com.zell_mbc.medilog.bloodpressure

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.preference.PreferenceManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import kotlinx.android.synthetic.main.bloodpressure_tab.*
import kotlinx.android.synthetic.main.bloodpressureeditform.*
import kotlinx.android.synthetic.main.bloodpressureeditform.etComment
import kotlinx.android.synthetic.main.bloodpressureeditform.etDia
import kotlinx.android.synthetic.main.bloodpressureeditform.etPulse
import kotlinx.android.synthetic.main.bloodpressureeditform.etSys
import java.text.DateFormat
import java.util.*

class BloodPressureEditFragment : Fragment() {
    private val viewModel: EditBloodPressureViewModel by viewModels()
    var _id: Int = 0 // id is already taken by Fragment class
    private val timestampCal = Calendar.getInstance()
    private var heartRhythmIssue = false

    private fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch(e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) Snackbar.make(requireView(), errorMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()

        return valueInt
    }


    private fun saveItem() {
        val editItem = viewModel.getItem(_id)
        if (editItem == null) {
            Snackbar.make(requireView(), "Unknown error!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Safely convert string values
        var value: Int = stringToInt(etSys.text.toString(), getString(R.string.sysMissing))
        if (value < 0) return
        editItem.sys = value

        value = stringToInt(etDia.text.toString(), getString(R.string.diaMissing))
        if (value < 0) return
        editItem.dia = value

        value = stringToInt(etPulse.text.toString(), getString(R.string.pulseMissing))
        if (value < 0) return
        editItem.pulse = value

        editItem.timestamp = timestampCal.timeInMillis
        editItem.comment = etComment.text.toString()
        editItem.state = if (heartRhythmIssue) 1 else 0

        viewModel.update(editItem)

        Snackbar.make(requireView(),getString(R.string.itemUpdated), Snackbar.LENGTH_LONG).setAction("Action", null).show()
        requireActivity().onBackPressed()
    }

    private fun setHeartRhythm() {
        heartRhythmIssue = !heartRhythmIssue
        if (heartRhythmIssue) btHeartRhythm.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDarkRed))
        else btHeartRhythm.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bloodpressureeditform, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

        when (sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))) {
            this.getString(R.string.green) -> btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> btSave.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }
        btSave.setColorFilter(Color.WHITE)

        viewModel.init()
        val editItem = viewModel.getItem(_id)
        if (editItem == null) {
            Snackbar.make(requireView(), "Unknown error!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        this.etSys.setText(editItem.sys.toString())
        this.etDia.setText(editItem.dia.toString())
        this.etPulse.setText(editItem.pulse.toString())
        heartRhythmIssue = editItem.state != 0
        if (heartRhythmIssue) btHeartRhythm.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryDarkRed))
        else btHeartRhythm.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))


        // Check if we are really editing or if this is a new value
        if (viewModel.isTmpItem(editItem.comment)) {
            this.etComment.setText("")
            this.etSys.setText("") // Make sure fields are empty to avoid the need to delete the default 0
            this.etDia.setText("")
            this.etPulse.setText("")
        }
        else this.etComment.setText(editItem.comment)


        this.etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(editItem.timestamp)
        this.etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(editItem.timestamp)

        // Respond to click events
        btSave.setOnClickListener { saveItem() }
        btHeartRhythm.setOnClickListener { setHeartRhythm() }

        // ---------------------
        // Date/Time picker section
        timestampCal.timeInMillis = editItem.timestamp

        // create an OnDateSetListener
        val dateListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            timestampCal.set(Calendar.YEAR, year)
            timestampCal.set(Calendar.MONTH, monthOfYear)
            timestampCal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

        val timeListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            timestampCal.set(Calendar.HOUR, hour)
            timestampCal.set(Calendar.MINUTE, minute)

            etDate.text = DateFormat.getDateInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
            etTime.text = DateFormat.getTimeInstance(DateFormat.SHORT).format(timestampCal.timeInMillis)
        }

//        btDatePicker!!.setOnClickListener(object : View.OnClickListener {
        etDate.setOnClickListener {
            DatePickerDialog(requireContext(),
                    dateListener,
                    timestampCal.get(Calendar.YEAR),
                    timestampCal.get(Calendar.MONTH),
                    timestampCal.get(Calendar.DAY_OF_MONTH)).show()
        }

        etTime.setOnClickListener {
            TimePickerDialog(requireContext(),
                    timeListener,
                    timestampCal.get(Calendar.HOUR_OF_DAY),
                    timestampCal.get(Calendar.MINUTE),
                    android.text.format.DateFormat.is24HourFormat(requireContext())).show()
        }

        // Make sure first field is highlighted and keyboard is open
        this.etSys.requestFocus()
    }

    fun newInstance(i: Int): BloodPressureEditFragment? {
        val f = BloodPressureEditFragment()
        f._id = i
        return f
    }

    override fun onPause() {
        super.onPause()
        MainActivity. resetReAuthenticationTimer(requireContext())
        viewModel.delete()
    }
}