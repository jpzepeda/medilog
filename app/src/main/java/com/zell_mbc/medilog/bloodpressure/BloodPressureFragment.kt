package com.zell_mbc.medilog.bloodpressure

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.*
import kotlinx.android.synthetic.main.bloodpressure_tab.*
import java.util.*
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.bloodpressure_tab.etComment
import kotlinx.android.synthetic.main.bloodpressure_tab.etDia
import kotlinx.android.synthetic.main.bloodpressure_tab.etPulse
import kotlinx.android.synthetic.main.bloodpressure_tab.etSys
import kotlinx.android.synthetic.main.bloodpressureeditform.*
import java.text.DateFormat

class BloodPressureFragment : Fragment(), BloodPressureListAdapter.ItemClickListener {
    private lateinit var viewModel: BloodPressureViewModel
    private var adapter: BloodPressureListAdapter? = null
    private var quickEntry = true
    private var standardColors = 0
    private var heartRhythmIssue = false


    private fun stringToInt(valueString: String, errorMessage: String): Int {
        var valueInt = -1
        if (valueString.isNotEmpty()) {
            try {
                valueInt = valueString.toInt()
                if (valueInt <= 9) valueInt = -2
            }
            catch(e: Exception) {
                valueInt = -3
            }
        }
        if  (valueInt < 1) Snackbar.make(requireView(), errorMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()

        return valueInt
    }


    private fun addItem() {

        if (!quickEntry) {
            val tmpComment = "temporaryEntry-YouShouldNeverSeeThis"
            val tmpItem = BloodPressure(0, Date().time, tmpComment, 0,0,0,0)
            val _id = viewModel.insert(tmpItem)
            if (_id  != 0L) editItem(_id.toInt())
            else Snackbar.make(requireView(), "No entry with tmp weight found!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // ###########################
        // Checks
        // ###########################
        // Savely convert string values
        var value: Int = stringToInt(etSys.text.toString(), getString(R.string.sysMissing))
        if (value < 0) return
        val iSys = value

        value = stringToInt(etDia.text.toString(), getString(R.string.diaMissing))
        if (value < 0) return
        val iDia = value

        value = stringToInt(etPulse.text.toString(), getString(R.string.pulseMissing))
        if (value < 0) return
        val iPulse = value

        val sC = etComment.text.toString()

        val item = BloodPressure(0, Date().time, sC, iSys, iDia, iPulse,0)

        viewModel.insert(item)
        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) Snackbar.make(requireView(), getString(R.string.filteredOut), 4000).setAction("Action", null).show()

        etSys.setText("")
        etDia.setText("")
        etPulse.setText("")
        etComment.setText("")
        etSys.bringToFront()
    }

    private fun editItem(index: Int) {
 //       Log.d("--------------- Debug", "Index: " + index)

        val intent = Intent(requireContext(), BloodPressureEditActivity::class.java)
        val extras = Bundle()
        val bp = viewModel.getItem(index)
//        Log.d("--------------- Debug", "bp: " + bp)
        if (bp != null) {
            extras.putInt("ID", bp._id)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun deleteItem(index: Int) {
        // Remove from array
//        Log.d("Debug BPFragment", "Delete: " + index)
        val item = viewModel.getItem(index)
        if (item != null) {
            viewModel.delete(item._id)
        }
        Snackbar.make(requireView(), viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_deleted), Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

/*    private fun setHeartRhythm() {
        heartRhythmIssue = !heartRhythmIssue
        if (heartRhythmIssue) btHeartRhythm.imageTintList
        else btHeartRhythm.imageTintList
    }
*/
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        Log.d("BloodPressureFragment onCreateView : ", "Start" + inflater + " parent " + container + " bundle " + savedInstanceState)
        //       super.onCreate(savedInstanceState)
        return inflater.inflate(R.layout.bloodpressure_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        Log.d("BloodPressureFragment onViewCreated : ", "Start")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        quickEntry = sharedPref.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)

        when (colourStyle) {
            this.getString(R.string.green) -> btAdd.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> btAdd.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> btAdd.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }

        // Hide quick entry fields if needed
        if (!quickEntry) {
            etSys.visibility = View.GONE
            etDia.visibility = View.GONE
            etPulse.visibility = View.GONE
            etComment.visibility = View.GONE
 //           btHeartRhythm.visibility = View.GONE
        }
        else {
            etSys.visibility = View.VISIBLE
            etDia.visibility = View.VISIBLE
            etPulse.visibility = View.VISIBLE
            etComment.visibility = View.VISIBLE
  //          btHeartRhythm.visibility = View.VISIBLE
        }

        // set up the RecyclerView
        val layoutManager = LinearLayoutManager(requireContext())
        rvBloodPressureList.layoutManager = layoutManager

        adapter = BloodPressureListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(BloodPressureViewModel::class.java)
        viewModel.items.observe(requireActivity(), { bloodPressure -> bloodPressure?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        rvBloodPressureList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(rvBloodPressureList.context, layoutManager.orientation)
        rvBloodPressureList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        btAdd.setOnClickListener { addItem() }

//        btHeartRhythm.setOnClickListener { setHeartRhythm() }

        when (colourStyle) {
            this.getString(R.string.green) -> btShowChart.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGreen))
            this.getString(R.string.blue)  -> btShowChart.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightBlue))
            this.getString(R.string.red)   -> btShowChart.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightRed))
        }
        btAdd.setColorFilter(Color.WHITE)
        standardColors =  etDia.currentHintTextColor //  textColors.defaultColor
//        btHeartRhythm.setTextColor(standardColors) // Make sure the button comes up in correct colour

        btShowChart.setOnClickListener(View.OnClickListener {
            val c = context ?: return@OnClickListener
            if (viewModel.getSize(true) < 2) {
                Snackbar.make(view, c.getString(R.string.notEnoughDataForChart), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return@OnClickListener
            }
            val intent = Intent(requireContext(), BloodPressureChartActivity::class.java)
            startActivity(intent)
        })
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        MaterialDialog(requireContext()).show {
            title(text = getString(R.string.ItemClicked) + " " + dateFormat.format(item.timestamp))
            message(R.string.whatDoYouWant)
            neutralButton(R.string.cancel)
            positiveButton(R.string.delete) { deleteItem(item._id) }
            negativeButton(R.string.edit) { editItem(item._id) }
        }
    }
}