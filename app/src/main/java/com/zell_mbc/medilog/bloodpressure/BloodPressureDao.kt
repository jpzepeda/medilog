package com.zell_mbc.medilog.bloodpressure

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.zell_mbc.medilog.BloodPressure
import com.zell_mbc.medilog.base.DataDao

@Dao
abstract class BloodPressureDao: DataDao<BloodPressure> {

    @Query("SELECT MAX(sys) FROM bloodpressure")
    abstract fun getMaxSys(): Int

    @Query("SELECT MAX(dia) FROM bloodpressure")
    abstract fun getMaxDia(): Int

    @Query("SELECT MIN(sys) FROM bloodpressure")
    abstract fun getMinSys(): Int

    @Query("SELECT MIN(dia) FROM bloodpressure")
    abstract fun getMinDia(): Int

    @Query("DELETE from bloodpressure where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from bloodpressure where comment = :commentValue")
    abstract suspend fun delete(commentValue: String)

    // ----------------------------
    @Query("SELECT COUNT(*) FROM bloodpressure")
    abstract fun getSize(): Int

    @Query("SELECT COUNT(*) FROM bloodpressure WHERE timestamp >=:start")
    abstract fun getSizeGreaterThan(start: Long): Int

    @Query("SELECT COUNT(*) FROM bloodpressure WHERE timestamp <=:end")
    abstract fun getSizeLessThan(end: Long): Int

    @Query("SELECT COUNT(*) FROM bloodpressure WHERE timestamp >=:start and timestamp <=:end")
    abstract fun getSizeRange(start: Long, end: Long): Int
    // ----------------------------

    @Query("SELECT * from bloodpressure where _id = :id")
    abstract fun getItem(id: Int): BloodPressure

    @Query("SELECT * from bloodpressure where comment = :commentValue")
    abstract fun getItem(commentValue: String): BloodPressure

    // ----------------------------
    // getBloodPressureItems options, return type List
    // ------------ASC ------------
    @Query("SELECT * from bloodpressure ORDER BY timestamp ASC") // Used by Chart fragment
    abstract fun getItemsASC(): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getItemsASCRange(start: Long, end: Long): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp >=:start ORDER BY timestamp ASC")
    abstract fun getItemsASCGreaterThan(start: Long): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getItemsASCLessThan(end: Long): List<BloodPressure>

    // ------------DESC ------------
    @Query("SELECT * from bloodpressure ORDER BY timestamp DESC") // Used by Recycler view
    abstract fun getItemsDESC(): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsDESCRange(start: Long, end: Long): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsDESCGreaterThan(start: Long): List<BloodPressure>

    @Query("SELECT * from bloodpressure WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsDESCLessThan(end: Long): List<BloodPressure>

    // ----------------------------
    // getBloodPressureItems options, return type LiveData
    // Recycler view
    @Query("SELECT * from bloodpressure WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsRange(start: Long, end: Long): LiveData<List<BloodPressure>>

    @Query("SELECT * from bloodpressure WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsGreaterThan(start: Long): LiveData<List<BloodPressure>>

    @Query("SELECT * from bloodpressure WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsLessThan(end: Long): LiveData<List<BloodPressure>>

    @Query("SELECT * from bloodpressure ORDER BY timestamp DESC")
    abstract fun getItems(): LiveData<List<BloodPressure>>
    // ----------------------

    @Query("DELETE FROM bloodpressure")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM bloodpressure WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)
}
