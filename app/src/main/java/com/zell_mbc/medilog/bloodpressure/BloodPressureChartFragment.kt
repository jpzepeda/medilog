package com.zell_mbc.medilog.bloodpressure

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Color.parseColor
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.androidplot.ui.*
import com.androidplot.xy.*
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import java.text.*
import java.util.*
import kotlin.math.roundToInt


class BloodPressureChartFragment : Fragment() {
    private var sysBand1 = ArrayList<Float>()
    private var sysBand2 = ArrayList<Float>()
    private var sysBand3 = ArrayList<Float>()
    var sys = ArrayList<Float>()

    private var diaBand1 = ArrayList<Float>()
    private var diaBand2 = ArrayList<Float>()
    private var diaBand3 = ArrayList<Float>()
    private var dia = ArrayList<Float>()
    private var sysLinearTrend = ArrayList<Float>()
    private var diaLinearTrend = ArrayList<Float>()
    private var sysSMATrend = ArrayList<Float>()
    private var diaSMATrend = ArrayList<Float>()
    private var period = 5 // Minimum value = 2

    private fun calculateMovingAverage(source: ArrayList<Float>, target: ArrayList<Float>) {
        val sample = Array(period) { 0f} // Create array of float, with all values set to 0
        val n = source.size
        var sma: Float

        // the first n values in the sma will be off -> set them to the first weight value
        for (i in 0 until period) {
            sample[i] = source[0]
        }

        for (i in 0 until n) {
            for (ii in 0..period-2) {
                sample[ii] = sample[ii + 1]
            }
            sample[period-1] = source[i]

            sma = 0f
            for (ii in 0 until period) {
                sma += sample[ii]
            }
            sma /= period
            target.add(sma)
        }
    }

    private fun calculateLinearTrendLine(source: ArrayList<Float>, target: ArrayList<Float>) {
        // https://classroom.synonym.com/calculate-trendline-2709.html
        var a = 0f
        val b: Float
        var b1 = 0
        var b2 = 0f
        var c = 0
        val d: Float
        val e: Float
        val f: Float
        val g: Float
        val m: Float
        val n = source.size
        for (i in 1..n) {
            a += i * source[i - 1]
            b1 += i
            b2 += source[i - 1]
            c += i * i
        }
        a *= n
        b = b1 * b2
        c *= n
        d = b1 * b1.toFloat()
        m = (a - b) / (c - d)
        e = b2
        f = m * b1
        g = (e - f) / n
        var value: Float
        for (i in 1..n) {
            value = m * i + g
            target.add(value)
            //            Log.d("--------------- Debug", "Value: " + value);
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bloodpressure_chart, container, false)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val c = context
        if (c == null) {
            Log.d("--------------- Debug", " Chart Empty Context")
            return
        }

        // create a couple arrays of y-values to plot:
        val labels = ArrayList<String>()
        val pulse = ArrayList<Float>()
        var wMax = 0f
        var wMin = 1000f

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(c)
        val daySteppingMode = sharedPref.getBoolean(SettingsActivity.KEY_PREF_bpDayStepping, false)
        val barChart = sharedPref.getBoolean(SettingsActivity.KEY_PREF_bpBarChart, false)
        val showPulse =sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWPULSE, true)

        var sTmp: String
        var fTmp: Float
        val lastDate = Calendar.getInstance()
        var currentDate: Date?
        val simpleDate = SimpleDateFormat("MM-dd")

        val viewModel = ViewModelProvider(this).get(BloodPressureViewModel::class.java)
        viewModel.init()
        val items = viewModel.getItems("ASC", filtered = true)
        for (bpI in items) {
            sTmp = simpleDate.format(bpI.timestamp)

            // Chart stepping by day
            if (daySteppingMode) {
                // Fill gap
                currentDate = try {
                    simpleDate.parse(sTmp)
                } catch (e: ParseException) {
                    continue
                }

                /*                   Log.d("--------------- Debug", "currentDate: " + currentDate);
                    Log.d("--------------- Debug", "lastDate: " + lastDate.getTime());
                    Log.d("--------------- Debug", "Compare: " + currentDate.compareTo(lastDate.getTime()));*/
                if (currentDate != null) {
                    while ((labels.size > 0) && (currentDate > lastDate.time)) {
                        sTmp = simpleDate.format(lastDate.time)
                        labels.add(sTmp)
                        sys.add(0f)
                        dia.add(0f)
                        if (showPulse) pulse.add(0f)
                        //                        Log.d("--------------- Debug", "Gap Date: " + sTmp);
                        lastDate.add(Calendar.DAY_OF_MONTH, 1)
                    }
                }
                if (currentDate != null) lastDate.time = currentDate
            }

            labels.add(sTmp)
            sys.add(bpI.sys.toFloat())
            fTmp = bpI.sys.toFloat()
            if (fTmp > wMax) {
                wMax = fTmp
            }

            dia.add(bpI.dia.toFloat())
            fTmp = bpI.dia.toFloat()
            if (fTmp < wMin) {
                wMin = fTmp
            }

            if (showPulse) {
                pulse.add(bpI.pulse.toFloat())
                fTmp = bpI.pulse.toFloat()
                if (fTmp < wMin) {
                    wMin = fTmp
                }
            }
        }

        if (sys.size == 0) {
            return
        }

        val threshold = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWTHRESHOLDS, false)
        if (threshold) {
            val bpHelper = BloodPressureHelper(c)
            val sysB1 = bpHelper.hyperGrade1Sys.toFloat()
            val sysB2 = bpHelper.hyperGrade2Sys.toFloat()
            val sysB3 = bpHelper.hyperGrade3Sys.toFloat()
            val diaB1 = bpHelper.hyperGrade1Dia.toFloat()
            val diaB2 = bpHelper.hyperGrade2Dia.toFloat()
            val diaB3 = bpHelper.hyperGrade3Dia.toFloat()
            for (item in sys) {
                sysBand1.add(sysB1)
                sysBand2.add(sysB2)
                sysBand3.add(sysB3)
                diaBand1.add(diaB1)
                diaBand2.add(diaB2)
                diaBand3.add(diaB3)
            }
        }

        // https://github.com/halfhp/androidplot/blob/master/docs/plot_composition.md
        // initialize our XYPlot reference:
        val plot: XYPlot = view.findViewById(R.id.bloodPressurePlot)

        val showGrid = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWGRID, true)
        if (!showGrid) {
            plot.graph.domainGridLinePaint = null
            plot.graph.rangeGridLinePaint = null
        }
        else {
            plot.graph.domainGridLinePaint = Paint(Color.GRAY)
            plot.graph.rangeGridLinePaint = Paint(Color.GRAY) // Horizontal lines
        }


        val isLegendVisible = sharedPref.getBoolean(SettingsActivity.KEY_PREF_SHOWLEGEND, true)
        plot.legend.isVisible = isLegendVisible
        plot.legend.setWidth(0.6f)

        plot.legend.position(-0.6f, HorizontalPositioning.RELATIVE_TO_RIGHT , 0f, VerticalPositioning.ABSOLUTE_FROM_BOTTOM,Anchor.LEFT_BOTTOM)
        plot.legend.setTableModel(DynamicTableModel(3, 1, TableOrder.ROW_MAJOR))

        val series1: XYSeries = SimpleXYSeries(sys, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.systolic))
        val series2: XYSeries = SimpleXYSeries(dia, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.diastolic))


        val series3: XYSeries = SimpleXYSeries(pulse, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, getString(R.string.pulse))

        // Line charts don't work without additional effort with date gaps, hence we switch to bar charts
        // Bar chart
        if (barChart) {
            val series1Format = BarFormatter(Color.BLUE, Color.BLUE)
            val series2Format = BarFormatter(Color.RED, Color.RED)
            val series3Format = BarFormatter(Color.GREEN, Color.GREEN)
            // add a new series' to the xyplot:
            plot.addSeries(series1, series1Format)
            plot.addSeries(series2, series2Format)
            if (showPulse) plot.addSeries(series3, series3Format)
        } else {
            val series1Format = LineAndPointFormatter(Color.BLUE, null, null, null)
            val series1FormatLight = LineAndPointFormatter(parseColor("#ADD8E6"), null, null, null)
            val series2Format = LineAndPointFormatter(Color.RED, null, null, null)
            val series2FormatLight = LineAndPointFormatter(parseColor("#FFA500"), null, null, null)
            val series3Format = LineAndPointFormatter(Color.GREEN, null, null, null)

            // add a new series' to the xyplot:
            series1Format.isLegendIconEnabled = isLegendVisible
            series2Format.isLegendIconEnabled = isLegendVisible
            series3Format.isLegendIconEnabled = isLegendVisible

            plot.addSeries(series1, series1Format)
            plot.addSeries(series2, series2Format)
            if (showPulse) plot.addSeries(series3, series3Format)

            if (threshold) {
                val series4: XYSeries = SimpleXYSeries(sysBand1, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "140")
                val series5: XYSeries = SimpleXYSeries(sysBand2, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "160")
                val series6: XYSeries = SimpleXYSeries(sysBand3, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "180")

                val series7: XYSeries = SimpleXYSeries(diaBand1, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "140")
                val series8: XYSeries = SimpleXYSeries(diaBand2, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "160")
                val series9: XYSeries = SimpleXYSeries(diaBand3, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "180")

                series1FormatLight.isLegendIconEnabled = false
                series2FormatLight.isLegendIconEnabled = false

                // Sys bands
                plot.addSeries(series4, series1FormatLight)
                plot.addSeries(series5, series1FormatLight)
                plot.addSeries(series6, series1FormatLight)
                // Dia bands
                plot.addSeries(series7, series2FormatLight)
                plot.addSeries(series8, series2FormatLight)
                plot.addSeries(series9, series2FormatLight)
            }
        }

//        plot.getLegend().setLegendItemComparator(myComparator);

        // Trendlines
        val bloodPressureLinarTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_LINEAR_TRENDLINE, false)
        val bloodPressureMovingAverageTrendLine = sharedPref.getBoolean(SettingsActivity.KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_TRENDLINE, false)

        if (bloodPressureLinarTrendLine) {
            calculateLinearTrendLine(sys, sysLinearTrend)
            calculateLinearTrendLine(dia, diaLinearTrend)

            val sysLinearTrendLine: XYSeries = SimpleXYSeries(sysLinearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val sysLinearTrendFormat = LineAndPointFormatter(Color.BLUE, null, null, null)
            sysLinearTrendFormat.isLegendIconEnabled = false
            plot.addSeries(sysLinearTrendLine, sysLinearTrendFormat)
            val diaLinearTrendLine: XYSeries = SimpleXYSeries(diaLinearTrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val diaLinearTrendFormat = LineAndPointFormatter(Color.RED, null, null, null)
            diaLinearTrendFormat.isLegendIconEnabled = false
            plot.addSeries(diaLinearTrendLine, diaLinearTrendFormat)
        }

        if (bloodPressureMovingAverageTrendLine) {
            period = sharedPref.getString(SettingsActivity.KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE, "5")!!.toInt()

            calculateMovingAverage(sys, sysSMATrend)
            calculateMovingAverage(dia, diaSMATrend)

            val sysMovingAverageTrendLine: XYSeries = SimpleXYSeries(sysSMATrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val sysMovingAverageTrendFormat = LineAndPointFormatter(Color.BLUE, null, null, null)
            sysMovingAverageTrendFormat.isLegendIconEnabled = false
            plot.addSeries(sysMovingAverageTrendLine, sysMovingAverageTrendFormat)
            val diaMovingAverageTrendLine: XYSeries = SimpleXYSeries(diaSMATrend, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, null)
            val diaMovingAverageTrendFormat = LineAndPointFormatter(Color.RED, null, null, null)


            diaMovingAverageTrendFormat.isLegendIconEnabled = false
            plot.addSeries(diaMovingAverageTrendLine, diaMovingAverageTrendFormat)
        }

        // Set boundaries to 10th at -20 and + 20 of min and max value
        val wMinBoundary = wMin.roundToInt() - 10
        val wMaxBoundary = wMax.roundToInt() + 10
        plot.setRangeBoundaries(wMinBoundary, wMaxBoundary, BoundaryMode.FIXED)
        plot.setUserRangeOrigin(wMinBoundary/10*10) // Set to a 10er value

        plot.setRangeStep(StepMode.INCREMENT_BY_VAL, 10.0)

        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("#") // Set integer y-Axis label
        plot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : Format() {
            override fun format(obj: Any, toAppendTo: StringBuffer, pos: FieldPosition): StringBuffer {
                val i = (obj as Number).toFloat().roundToInt()
                return toAppendTo.append(labels[i])
            }

            override fun parseObject(source: String, pos: ParsePosition): Any {
                return 0
            }
        }
    }
}

/*
 plot.setDrawingCacheEnabled(true);
FileOutputStream fos = new FileOutputStream(“/sdcard/DCIM/img.png”, true);
plot.getDrawingCache().compress(CompressFormat.PNG, 100, fos);
fos.close();
plot.setDrawingCacheEnabled(false);
 */