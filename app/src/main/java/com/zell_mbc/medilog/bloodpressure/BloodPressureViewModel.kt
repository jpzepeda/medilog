package com.zell_mbc.medilog.bloodpressure

import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.base.DataViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.BufferedReader
import java.io.IOException
import java.text.DateFormat
import java.text.ParseException
import java.util.*
import kotlin.collections.ArrayList

class BloodPressureViewModel(application: Application): DataViewModel(application) {
    override val fileName = "BloodPressure.csv"
    override var itemName = app.getString(R.string.bloodPressure)
    override val filterStartPref = "BLOODPRESSUREFILTERSTART"
    override val filterEndPref = "BLOODPRESSUREFILTEREND"

    override lateinit var repository: BloodPressureRepository
    private lateinit var dao: BloodPressureDao
    lateinit var items: LiveData<List<BloodPressure>>

    override fun init() {
        super.init()
        dao = MediLogDB.getDatabase(app).bloodPressureDao()
        repository = BloodPressureRepository(dao, filterStart, filterEnd)
//        items = repository.items
        items = Transformations.switchMap(filterChanged) { repository.getItems(filterStart, filterEnd) }
    }

    fun delete(commentValue: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(commentValue)
    }

    override fun getItem(id: Int): BloodPressure? {
        var item: BloodPressure? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(id) }
            j.join() }
        return item
    }

    fun getItem(commentValue: String): BloodPressure? {
        var item: BloodPressure? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(commentValue) }
            j.join() }
        return item
    }

    override fun getItems(order: String, filtered: Boolean): List<BloodPressure> {
        if (filtered) getFilter()
        lateinit var items: List<BloodPressure>
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            items = repository.getItems(order, filterStart, filterEnd) }
            j.join() }
        return items
    }

    fun insert(item: BloodPressure): Long {
        var rowId = -1L
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            rowId = repository.insert(item)
//            rowId = repository.getIdFromRowId(tmpL) // Translate from DB row to _id column
        }
            j.join() }
        return rowId
    }

    fun update(bloodPressure: BloodPressure) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(bloodPressure)
    }

    fun getMaxSys(): Int {
        var i = 0
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            i = repository.getMaxSys() }
            j.join() }
        return i
    }

    fun getMinSys(): Int {
        var i = 0
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            i = repository.getMinSys() }
            j.join() }
        return i
    }

    fun getMaxDia(): Int {
        var i = 0
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            i = repository.getMaxDia() }
            j.join() }
        return i
    }

    fun getMinDia(): Int {
        var i = 0
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            i = repository.getMinDia() }
            j.join() }
        return i
    }

// --------

    private fun csvToItem(bp: BloodPressure, csv: String): Int {
        val data = csv.split(separator!!.toRegex()).toTypedArray()

        // At a minimum 4 values need to be present, comment is optional
        if (data.size < 4) {
            return -1
        }
        try {
            val date: Date? = csvPattern.parse(data[0].trim())
            if (date != null) bp.timestamp = date.time
        } catch (e: ParseException) {
            return -2
        }
        try {
            bp.sys = data[1].trim().toInt()
            bp.dia = data[2].trim().toInt()
            bp.pulse = data[3].trim().toInt()
        } catch (e: NumberFormatException) {
            return -3
        }

        // Comment not always available
        if (data.size == 5) {
            bp.comment = data[4]
        } else {
            bp.comment = ""
        }
        return 0
    }


    override suspend fun importFile(reader: BufferedReader?, replaceData: Boolean): Int {
        val tmpItems = ArrayList<BloodPressure>()
        if (reader == null) {
            return 0
        }
        try {
            var tmpLine: String
            var showError = true
            var line: String?
            var lineNo = 0
            while (reader.readLine().also { line = it } != null) {
                lineNo++
                val item = BloodPressure(0, 0, "", 0,0, 0,0) // Start with empty item
                // At a minimum 2 values need to be present, comment is optional
//                Log.d("importFile", "Line" + line)
                tmpLine = line.toString()
                val err = csvToItem(item, line!!)
                if (err < 0) {
                    if (showError) {
                        Handler(Looper.getMainLooper()).post {
                            Toast.makeText(app, "Error $err importing line $lineNo, '$tmpLine'", Toast.LENGTH_LONG).show()
                        }
                        showError = false // Show only first error to avoid pushing hundreds of similar errors
                    }
                    continue
                }
                tmpItems.add(item)
            }
        } catch (e: IOException) {
            return -1
        }

        return transferItems(tmpItems, replaceData)
    }


    private suspend fun transferItems(tmpItems: ArrayList<BloodPressure>, replaceData: Boolean): Int {
        val records = tmpItems.size
        if (records == 0) return -2     // Delete only after a successful import

        // Delete only after a successful import
        if (replaceData) {
            deleteAll(false)
            // Wait until DB is empty
            val i = 20
            while (getSize(false) > 0 && i > 0) {
                //                   Log.d("SuspendedInsert", "Avoid race condition -> Wait 100ms")
                delay(100L)
                // Log.d("--------------- Debug", "Delay:" + i--)
                if (i == 0) {
                    // If after 10 rounds the DB is not empty something odd is going on
                    Handler(Looper.getMainLooper()).post { Toast.makeText(app, "Error! Unable to delete all old records. ", Toast.LENGTH_LONG).show() }
                }
            }
        }
        for (item in tmpItems) {
            insert(item)
        }
        return records
    }

    override fun getCsvData(filtered: Boolean): String {
        val sb = StringBuilder()

        // CSV header
        sb.append(app.getString(R.string.date) + separator + app.getString(R.string.systolic) + separator + app.getString(R.string.diastolic) + separator + app.getString(R.string.pulse) + separator + app.getString(R.string.comment) + System.getProperty("line.separator"))

        // Data
        val items = getItems("DESC", filtered)
        for (item in items) {
            sb.append(csvPattern.format(item.timestamp) + separator + item.sys + separator + item.dia + separator + item.pulse + separator + item.comment + System.getProperty("line.separator"))
        }
        return sb.toString()
    }

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            Toast.makeText(app, app.getString(R.string.bloodPressure) + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }

        val userName = sharedPref.getString(SettingsActivity.KEY_PREF_USER, "")
        val highlightValues = sharedPref.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        val bpHelper = BloodPressureHelper(app)

        val document = PdfDocument()

        val diaTab = 30
        val section1 = 130
        val section2 = 185
        val section3 = 250
        val pulseTab = 310
        val commentTab = 350
        val pn = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas
        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.isFakeBoldText = false
        pdfPaint.color = Color.BLACK

        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true

        val paintRed = Paint()
        paintRed.color = Color.RED

        // Black & White instead of colour
        val blackAndWhite = true

        // -----------
        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pn).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas

        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        // Draw header
        var headerText = app.getString(R.string.bpReportTitle)
        if (userName!!.isNotEmpty()) {
            headerText = headerText + " " + app.getString(R.string.forString) + " " + userName
        }
        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        // Data section
        i = pdfDataTop
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
//        canvas.drawText(app.getString(R.string.time), pdfTimeTab.toFloat(), i, pdfPaintHighlight);
        canvas.drawText(app.getString(R.string.morning), section1.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.afternoon), section2.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.evening), section3.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.pulse), pulseTab.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.comment), commentTab.toFloat(), i.toFloat(), pdfPaintHighlight)
        // ------------
        val space = 5
        pdfPaint.color = Color.DKGRAY
        canvas.drawLine(section1 - space.toFloat(), pdfHeaderBottom.toFloat(), section1 - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        canvas.drawLine(section2 - space.toFloat(), pdfHeaderBottom.toFloat(), section2 - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        canvas.drawLine(section3 - space.toFloat(), pdfHeaderBottom.toFloat(), section3 - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        pdfPaint.color = Color.BLACK
        canvas.drawLine(pulseTab - space.toFloat(), pdfHeaderBottom.toFloat(), pulseTab - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
        canvas.drawLine(commentTab - space.toFloat(), pdfHeaderBottom.toFloat(), commentTab - space.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        val items = getItems("ASC", filtered)
        for (item in items) {
            i += pdfLineSpacing
            // Start new page
            if (i > pdfDataBottom) {
                document.finishPage(page)

                // -----------
                // crate a A4 page description
                pageInfo = PdfDocument.PageInfo.Builder(595, 842, pn).create()
                page = document.startPage(pageInfo)
                canvas = page.canvas
//                dataBottom = canvas.height - 15
                canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                i = pdfDataTop
                canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
                //               canvas.drawText(app.getString(R.string.time), timeTab, i, paint);
                canvas.drawText(app.getString(R.string.morning), section1.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.afternoon), section2.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.evening), section3.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.pulse), pulseTab.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.comment), commentTab.toFloat(), i.toFloat(), pdfPaint)
                // ------------
                i += pdfLineSpacing
            }
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
            canvas.drawText(toStringTime(item.timestamp), pdfTimeTab.toFloat(), i.toFloat(), pdfPaint)
            //            Log.d("--------------- Debug", " " + ss);
            val dayPeriod = dayPeriod(item)
            var activeSection = section1
            if (dayPeriod == 1) {
                activeSection = section2
            }
            if (dayPeriod == 2) {
                activeSection = section3
            }
            if (highlightValues) {
                if (blackAndWhite) {
                    when(bpHelper.sysGrade(item.sys)) {
                        bpHelper.hyperGrade3 -> {
                            pdfPaint.isFakeBoldText = true
                            pdfPaint.isUnderlineText = true
                        }
                        bpHelper.hyperGrade2 -> {
                            pdfPaint.isFakeBoldText = true
                            pdfPaint.isUnderlineText = true
                        }
                        bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                        else                 -> pdfPaint.isFakeBoldText = false
                    }
                    canvas.drawText(item.sys.toString(), activeSection.toFloat(), i.toFloat(), pdfPaint)
                    pdfPaint.isFakeBoldText = false
                    pdfPaint.isUnderlineText = false

                    when(bpHelper.diaGrade(item.dia)) {
                        bpHelper.hyperGrade3 -> {
                            pdfPaint.isFakeBoldText = true
                            pdfPaint.isUnderlineText = true
                        }
                        bpHelper.hyperGrade2 -> {
                            pdfPaint.isFakeBoldText = true
                            pdfPaint.isUnderlineText = true
                        }
                        bpHelper.hyperGrade1 -> pdfPaint.isFakeBoldText = true
                        else                 -> pdfPaint.isFakeBoldText = false
                    }
                    canvas.drawText(item.dia.toString(), activeSection + diaTab.toFloat(), i.toFloat(), pdfPaint)
                    pdfPaint.isFakeBoldText = false
                    pdfPaint.isUnderlineText = false
                } // blackAndWhite
                else {
                    when(bpHelper.sysGrade(item.sys)) {
                        bpHelper.hyperGrade3 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade2 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade1 -> pdfPaint.color = Color.rgb(255, 165, 0)
                        else                 -> pdfPaint.color = Color.BLACK
                    }
                    canvas.drawText(item.sys.toString(), activeSection.toFloat(), i.toFloat(), pdfPaint)

                    when(bpHelper.diaGrade(item.dia)) {
                        bpHelper.hyperGrade3 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade2 -> pdfPaint.color = Color.RED
                        bpHelper.hyperGrade1 -> pdfPaint.color = Color.rgb(255, 165, 0)
                        else                    -> pdfPaint.color = Color.BLACK
                    }
                    canvas.drawText(item.dia.toString(), activeSection + diaTab.toFloat(), i.toFloat(), pdfPaint)
                    pdfPaint.color = Color.BLACK
                }
            }
            else {
                canvas.drawText(item.sys.toString(), activeSection.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(item.dia.toString(), activeSection + diaTab.toFloat(), i.toFloat(), pdfPaint)
            }
            canvas.drawText(item.pulse.toString(), pulseTab.toFloat(), i.toFloat(), pdfPaint)
            canvas.drawText(item.comment, commentTab.toFloat(), i.toFloat(), pdfPaint)
        }
        // finish the page
        document.finishPage(page)

        //      FileHelper fh = new FileHelper(app, fileName);
        return document
    }

    private fun dayPeriod(item: BloodPressure): Int {
        val p: Int
        val cal = Calendar.getInstance()
        cal.timeInMillis = item.timestamp
        val hour = cal[Calendar.HOUR_OF_DAY]
        p = when {
            hour <= 11 -> {
                0
            } // Morning
            hour <= 17 -> {
                1
            } // Afternoon
            else -> {
                2
            }
        } // Evening
        return p
    }
}
