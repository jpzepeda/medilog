package com.zell_mbc.medilog

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.PromptInfo
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getColor
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.zell_mbc.medilog.R.*
import com.zell_mbc.medilog.base.DataViewModel
import com.zell_mbc.medilog.bloodpressure.BloodPressureFragment
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.diary.DiaryFragment
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.water.WaterFragment
import com.zell_mbc.medilog.water.WaterViewModel
import com.zell_mbc.medilog.weight.WeightFragment
import com.zell_mbc.medilog.weight.WeightViewModel
import kotlinx.android.synthetic.main.activity_main.*
import net.lingala.zip4j.io.inputstream.ZipInputStream
import net.lingala.zip4j.io.outputstream.ZipOutputStream
import net.lingala.zip4j.model.LocalFileHeader
import net.lingala.zip4j.model.ZipParameters
import net.lingala.zip4j.model.enums.EncryptionMethod
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    lateinit var mediLog: MediLog

    private val rcRESTORE = 9998
    private val rcBACKUP = 9999

    //    lateval mainViewModel: ViewModel
    private lateinit var weightViewModel: WeightViewModel
    private lateinit var bloodPressureViewModel: BloodPressureViewModel
    private lateinit var diaryViewModel: DiaryViewModel
    private lateinit var waterViewModel: WaterViewModel

    // Fragments
    private var bloodPressureFragment: BloodPressureFragment? = null
    private var weightFragment: WeightFragment? = null
    private var diaryFragment: DiaryFragment? = null
    private var waterFragment: WaterFragment? = null

    // Initialize with negative value to clearly differentiate from tab values
    private var bloodPressureFragmentTabID = -1
    private var weightFragmentTabID = -1
    private var diaryFragmentTabID = -1
    private var waterFragmentTabID = -1

    private var hideMenus = false
    private val mContext: Context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mediLog = applicationContext as MediLog
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        if (!sharedPref.getBoolean("DISCLAIMERCONFIRMED", false)) {
            MaterialDialog(this).show {
                title(string.disclaimer)
                message(string.disclaimerText)
                positiveButton(string.agree) {
                    val editor = sharedPref.edit()
                    editor.putBoolean("DISCLAIMERCONFIRMED", true)
                    editor.apply()
                }
            }
        }

        // Initialize with blue
        if (sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, "").isNullOrEmpty()) {
            //          val themes = resources.getStringArray(R.array.themeOptions)
            val editor = sharedPref.edit()
            editor.putString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(string.blue))
            editor.apply()
        }

        waterViewModel = ViewModelProvider(this).get(WaterViewModel::class.java)
        waterViewModel.init()

        diaryViewModel = ViewModelProvider(this).get(DiaryViewModel::class.java)
        diaryViewModel.init()

        weightViewModel = ViewModelProvider(this).get(WeightViewModel::class.java)
        weightViewModel.init()

        bloodPressureViewModel = ViewModelProvider(this).get(BloodPressureViewModel::class.java)
        bloodPressureViewModel.init()

        setTheme()
        setContentView(layout.activity_main)

        val showBloodPressureTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureTab, true)
        val showWeightTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWeightTab, true)
        val showDiaryTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showDiaryTab, true)
        val showWaterTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWaterTab, false) // Don't show by default -> experimental
        val activeTab = sharedPref.getInt("activeTab", 0)

        // Initialize delimiter on first run based on locale
        var delimiter = sharedPref.getString(SettingsActivity.KEY_PREF_DELIMITER, "")
        if (delimiter.equals("")) {
            val currentLanguage = Locale.getDefault().displayLanguage
            delimiter = if (currentLanguage.contains("Deutsch")) {
                // Central Europe
                ";"
            } else ","

            val editor = sharedPref.edit()
            editor.putString(SettingsActivity.KEY_PREF_DELIMITER, delimiter)
            editor.apply()
        }

        hideMenus = false

        val adapter = TabAdapter(supportFragmentManager)

        if (showWeightTab) {
            weightFragment = WeightFragment()
            adapter.addFragment(weightFragment!!, getString(string.tab_weight))
            weightFragmentTabID = adapter.count - 1
        }
        if (showBloodPressureTab) {
            bloodPressureFragment = BloodPressureFragment()
            if (bloodPressureFragment != null)
                adapter.addFragment(bloodPressureFragment!!, getString(string.tab_bloodpressure))
            bloodPressureFragmentTabID = adapter.count - 1
        }
        if (showDiaryTab) {
            diaryFragment = DiaryFragment()
            adapter.addFragment(diaryFragment!!, getString(string.tab_diary))
            diaryFragmentTabID = adapter.count - 1
        }
        if (showWaterTab) {
            waterFragment = WaterFragment()
            adapter.addFragment(waterFragment!!, getString(string.tab_water))
            waterFragmentTabID = adapter.count - 1
        }

        // No active tabs?
        if (!(showWaterTab || showWeightTab || showBloodPressureTab || showDiaryTab)) {
            hideMenus = true
            Snackbar.make(findViewById(android.R.id.content), getString(string.noActiveTab), Snackbar.LENGTH_LONG).setAction("Action", null).show()
        }
        viewPager.adapter = adapter
        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                invalidateOptionsMenu()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        tabLayout.setupWithViewPager(viewPager)
        viewPager.currentItem = activeTab
        val toolbar = findViewById<Toolbar>(id.toolbar)
        setSupportActionBar(toolbar)
    }


    override fun onStart() {
//        Log.d("MainActivity", "onStart")
        super.onStart()

        checkTimeout()
    }

    private fun checkTimeout() {
    val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

    if (!sharedPref.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) {
        mediLog.setAuthenticated(true)
        return
    }

    // Check timer
    val now = Calendar.getInstance().timeInMillis
    val threshold = sharedPref.getLong("FORCE_REAUTHENTICATION", 300000) // 5 minutes
    val then = sharedPref.getLong("STOPWATCH", 0L)
    val diff = now - then

    // First check if authentication is enabled
    // If yes, check if re-authentication needs to be forced = if more than threshold milliseconds passed after last onStop event was executed
    if (diff > threshold)
    {
        mediLog.setAuthenticated(false)
    }

    val biometricHelper = BiometricHelper(this)
    val canAuthenticate = biometricHelper.canAuthenticate(true)
    if (!mediLog.isAuthenticated() && canAuthenticate == 0)
    {
        // No idea why I need this
        val newExecutor: Executor = Executors.newSingleThreadExecutor()
        val activity: FragmentActivity = this
        val myBiometricPrompt = BiometricPrompt(activity, newExecutor, object : BiometricPrompt.AuthenticationCallback() {

            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                val snackbar = Snackbar
                        .make(findViewById(android.R.id.content), "$errString " + getString(string.retryActionText), 300000)
                        .setAction(getString(string.retryAction)) { onStart() }
                snackbar.setActionTextColor(Color.RED)
                snackbar.show()
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                mediLog.setAuthenticated(true)
                runOnUiThread { viewPager?.visibility = View.VISIBLE }
            }

        })
        runOnUiThread { viewPager?.visibility = View.INVISIBLE }
        if (Build.VERSION.SDK_INT > 29) {
            val promptInfo = PromptInfo.Builder()
                    .setTitle(getString(string.app_name))
                    .setDeviceCredentialAllowed(true)  // Allow to use pin as well
                    .setSubtitle(getString(string.biometricLogin))
                    .setConfirmationRequired(false)
                    .build()
            myBiometricPrompt.authenticate(promptInfo)
        } else {
            // ToDo: Pin fallback Disabled for now until bug https://issuetracker.google.com/issues/142740104 is fixed)
            val promptInfo = PromptInfo.Builder()
                    .setTitle(getString(string.app_name))
                    .setNegativeButtonText(getString(string.cancel))
                    .setSubtitle(getString(string.biometricLogin)) //                        .setDescription(getString(R.string.biometricLogin)
                    .setConfirmationRequired(false)
                    .build()
            myBiometricPrompt.authenticate(promptInfo)
        }
    }
}
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        var filterActive = false

        viewPager.currentItem = tabLayout.selectedTabPosition
        when (tabLayout.selectedTabPosition) {
            bloodPressureFragmentTabID -> filterActive = ((bloodPressureViewModel.filterStart + bloodPressureViewModel.filterStart) > 0L)
            weightFragmentTabID -> filterActive = ((weightViewModel.filterStart + weightViewModel.filterEnd) > 0L)
            diaryFragmentTabID -> filterActive = ((diaryViewModel.filterStart + diaryViewModel.filterEnd) > 0L)
            waterFragmentTabID -> filterActive = ((waterViewModel.filterStart + waterViewModel.filterEnd) > 0L)
        }
//        Log.d("MainActivity", "onCreateOptionsMenu: filterstart " + filterStart + " filterEnd " + filterEnd)


        val filterIcon = menu.findItem(id.action_setFilter).icon
        filterIcon.mutate()
        if (filterActive) { filterIcon.setTint(getColor(this, color.colorActiveFilter)) }

        // Disable certain menus if no active Tab exists
        if (hideMenus) {
            menu.findItem(id.action_dataManagement).isVisible = false
            menu.findItem(id.action_send).isVisible = false
        }
        /*        else {
            menu.findItem(R.id.action_dataManagement).setVisible(true);
            menu.findItem(R.id.action_send).setVisible(false);
       }*/
        return true
    }

    // Menue
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val activeTab = tabLayout?.selectedTabPosition
        when (id) {
            R.id.action_setFilter -> {
                val bundle = Bundle()

                lateinit var vm: DataViewModel
                when (tabLayout.selectedTabPosition) {
                    waterFragmentTabID -> {
                        bundle.putString("activeTab", getString(string.tab_water))
                        vm = waterViewModel
                    }
                    weightFragmentTabID -> {
                        vm = weightViewModel
                        bundle.putString("activeTab", getString(string.tab_weight))
                    }
                    bloodPressureFragmentTabID -> {
                        vm = bloodPressureViewModel
                        bundle.putString("activeTab", getString(string.tab_bloodpressure))
                    }
                    diaryFragmentTabID -> {
                        bundle.putString("activeTab", getString(string.tab_diary))
                        vm = diaryViewModel
                    }
                }

                if (vm.getSize(false) == 0) {
                    Snackbar.make(findViewById(android.R.id.content), getString(string.emptyTable), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                    return  true
                }


                val dialogFragment = FilterFragment()
                dialogFragment.arguments = bundle

                val ft = supportFragmentManager.beginTransaction()
                val prev: Fragment? = supportFragmentManager.findFragmentByTag("FilterFragment")
                if (prev != null) {
                    ft.remove(prev)
                }
                ft.addToBackStack(null)
                dialogFragment.show(ft, "FilterFragment")

                return true
            }

            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }

            R.id.actionSendZIP -> {
                val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
                val zipPassword = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                if (zipPassword.isNullOrEmpty()) getSendZipPassword()
                else return sendZIP(zipPassword)
            }
            R.id.actionSendReport -> {
                val intent = Intent(Intent.ACTION_VIEW)
                var uri: Uri? = null
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

                when (activeTab) {
                    bloodPressureFragmentTabID -> uri = bloodPressureViewModel.getPdfFile(bloodPressureViewModel.createPdfDocument(true))
                    weightFragmentTabID -> uri = weightViewModel.getPdfFile(weightViewModel.createPdfDocument(true))
                    waterFragmentTabID -> uri = waterViewModel.getPdfFile(waterViewModel.createPdfDocument(true))
                    diaryFragmentTabID -> uri = diaryViewModel.getPdfFile(diaryViewModel.createPdfDocument(true))
                }

                if (uri != null) {
                    // Need to explicitly ask for permission once
                    val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    if (!hasPermissions(mContext, *PERMISSIONS)) {
                        ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                    }

                    intent.setDataAndType(uri, "application/*")
                    val pm = packageManager
                    if (intent.resolveActivity(pm) != null) {
                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                            Snackbar.make(findViewById(android.R.id.content), getString(string.eShareError) + ": " + e.localizedMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                        }
                    }
                }
                return true
            }
            R.id.actionSendCsvData -> {
                val intent = Intent(Intent.ACTION_VIEW)
                var uri: Uri? = null
                intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION

                when (activeTab) {
                    bloodPressureFragmentTabID -> uri = bloodPressureViewModel.getCsvFile(true)
                    weightFragmentTabID -> uri = weightViewModel.getCsvFile(true)
                    diaryFragmentTabID -> uri = diaryViewModel.getCsvFile(true)
                    waterFragmentTabID -> uri = waterViewModel.getCsvFile(true)
                }

                if (uri != null) {
                    // Need to explicitly ask for permission once
                    val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    if (!hasPermissions(mContext, *PERMISSIONS)) {
                        ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                    }
                    val mimeType = contentResolver.getType(uri)
                    intent.setDataAndType(uri, mimeType)

                    val pm = packageManager
                    if (intent.resolveActivity(pm) != null) {
                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                            Snackbar.make(findViewById(android.R.id.content), getString(string.eShareError) + ": " + e.localizedMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                        }
                    }
                }
                return true
            }

            R.id.action_deleteData -> {
                lateinit var vm: DataViewModel
                when (activeTab) {
                    waterFragmentTabID -> vm = waterViewModel
                    weightFragmentTabID -> vm = weightViewModel
                    bloodPressureFragmentTabID -> vm = bloodPressureViewModel
                    diaryFragmentTabID -> vm = diaryViewModel
                }

                if (vm.getSize(true) > 0) {
                    MaterialDialog(this).show {
                        title(string.warning)
                        message(text = getString(string.doYouReallyWantToContinue1) + " " + vm.getSize(true) + " " + getString(string.doYouReallyWantToContinue2) + " " + vm.itemName + " " + getString(string.doYouReallyWantToContinue3))
                        negativeButton(string.cancel)
                        positiveButton(string.yes) {
                            Toast.makeText(context, vm.getSize(true).toString() + " " + vm.itemName + " " + getString(string.recordsDeleted), Toast.LENGTH_SHORT).show()
                            vm.deleteAll(true)
                            vm.resetFilter()
                            setFilterIcon()
                        }
                    }
                } else Snackbar.make(findViewById(android.R.id.content), getString(string.noDataToDelete), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return true
            }
            R.id.action_restore -> {

                // Need to explicitly ask for permission once
                val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *PERMISSIONS)) {
                    ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                }

                // Pick backup folder
                var chooseFile = Intent(Intent.ACTION_GET_CONTENT)
                chooseFile.type = "*/*"
                chooseFile = Intent.createChooser(chooseFile, this.getString(string.selectFile))
                startActivityForResult(chooseFile, rcRESTORE)
                return true
            }
            R.id.action_backup -> {

                // Need to explicitly ask for permission once
                val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (!hasPermissions(mContext, *PERMISSIONS)) {
                    ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
                }
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.selectDirectory), Snackbar.LENGTH_LONG).setAction("Action", null).show()
//                Toast.makeText(this, this.getString(string.selectDirectory), Toast.LENGTH_LONG).show()

                // Pick backup folder
                val i = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
                i.addCategory(Intent.CATEGORY_DEFAULT)
                startActivityForResult(Intent.createChooser(i, this.getString(string.selectDirectory)), rcBACKUP)
                return true
            }
            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun sendZIP(zipPassword: String): Boolean {
        // Need to explicitly ask for permission once
        val PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!hasPermissions(mContext, *PERMISSIONS)) {
            ActivityCompat.requestPermissions((mContext as Activity), PERMISSIONS, REQUEST)
        }

        val intent = Intent(Intent.ACTION_SEND)
        val uri: Uri?
        intent.type = "application/zip"
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(Intent.EXTRA_TEXT, getString(string.sendText))

        lateinit var vm: DataViewModel
        when (tabLayout?.selectedTabPosition) {
            waterFragmentTabID -> vm = waterViewModel
            weightFragmentTabID -> vm = weightViewModel
            bloodPressureFragmentTabID -> vm = bloodPressureViewModel
            diaryFragmentTabID -> vm = diaryViewModel
        }

        intent.putExtra(Intent.EXTRA_SUBJECT, "MediLog " + vm.itemName + " " + getString(string.data))
        uri = vm.getZIP(vm.createPdfDocument(true), zipPassword)
        intent.putExtra(Intent.EXTRA_STREAM, uri)

        if ((uri != null) && (intent.resolveActivity(packageManager) != null)) {
            try {
                startActivity(Intent.createChooser(intent, getString(string.sendText)))
            } catch (e: Exception) {
                Snackbar.make(findViewById(android.R.id.content), getString(string.eShareError) + ": " + e.localizedMessage, Snackbar.LENGTH_LONG).setAction("Action", null).show()
//                        Toast.makeText(this,getString(string.eShareError) + ": " + e.localizedMessage, Toast.LENGTH_LONG).show()
                Log.e("Exception", getString(string.eShareError) + e.toString())
            }
        }
        return true
    }


    private fun getSendZipPassword() {
        MaterialDialog(this).show {
            title(string.enterPassword)
            input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                sendZIP(text.toString())
            }
            positiveButton(string.submit)
            negativeButton(string.cancel)
        }
    }

    private fun getOpenZipPassword(zipUri: Uri) {
        MaterialDialog(this).show {
            title(string.enterPassword)
            input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                openZIP(zipUri, text.toString())
            }
            positiveButton(string.submit)
            negativeButton(string.cancel)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //do here
            } else {
                Snackbar.make(findViewById(android.R.id.content), "E: MediLog  was not allowed to write to your file system.", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        if (data == null) return
        data ?: return

        // Backup section
        if (requestCode == rcBACKUP) {
            var backupFolderUri: Uri? = null

            // Check Uri is sound
            try {
                backupFolderUri = data.data
                Log.e("Debug", "Empty Uri $backupFolderUri")
            } catch (e: Exception) {
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.eSelectDirectory) + " " + data, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                //Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }
            if (backupFolderUri == null) {
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.eSelectDirectory) + " " + data, Snackbar.LENGTH_LONG).setAction("Action", null).show()
//                Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                return
            }
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
            val zipBackup = sharedPref.getBoolean(SettingsActivity.KEY_PREF_ZIPBACKUP, false)
            if (!zipBackup) {
                waterViewModel.exportCSV(backupFolderUri, waterViewModel.getCsvData(false))
                weightViewModel.exportCSV(backupFolderUri, weightViewModel.getCsvData(false))
                diaryViewModel.exportCSV(backupFolderUri, diaryViewModel.getCsvData(false))
                bloodPressureViewModel.exportCSV(backupFolderUri, bloodPressureViewModel.getCsvData(false))
            } else {
                val zipPassword = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                if (zipPassword.isNullOrEmpty()) {
                    MaterialDialog(this).show {
                        title(string.enterPassword)
                        input(inputType = InputType.TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD, allowEmpty = true, hintRes = string.password) { _, text ->
                            createZIP(backupFolderUri, text.toString())
                        }
                        positiveButton(string.submit)
                        negativeButton(string.cancel)
                    }
                }
                else createZIP(backupFolderUri, zipPassword)
            }
        }

        // Restore from backup
        if (requestCode == rcRESTORE) {
            var backupFileUri: Uri? = null
            try {
                backupFileUri = data.data
            } catch (e: Exception) {
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.eSelectDirectory) + " " + data, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                //Toast.makeText(this, this.getString(string.eSelectDirectory) + " " + data, Toast.LENGTH_LONG).show()
                //Log.e("Debug", "Invalid Uri: $e")
            }
            if (backupFileUri == null) {
                return
            }

            // Analyse file
            val cr = contentResolver

            when (val mimeType = cr.getType(backupFileUri)) {
                "application/zip" -> {
                    val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
                    val zipPassword = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD, "")
                    if (zipPassword.isNullOrEmpty()) getOpenZipPassword(backupFileUri)
                    else openZIP(backupFileUri, zipPassword)
                    return
                }
                "application/csv",
                "text/csv",
                "text/comma-separated-values" -> {
                    val inStream: InputStream?
                    try {
                        inStream = contentResolver.openInputStream(backupFileUri)
                        if (inStream == null) {
                            Snackbar.make(findViewById(android.R.id.content), this.getString(string.fileNotFound) + " " + backupFileUri, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                            return
                        }
                    } catch (e: FileNotFoundException) {
                        Snackbar.make(findViewById(android.R.id.content), this.getString(string.fileNotFound) + " " + backupFileUri, Snackbar.LENGTH_LONG).setAction("Action", null).show()
                        return
                    }
                    restoreData(inStream)
                    return
                }
                else -> {
                    Snackbar.make(findViewById(android.R.id.content), "Don't know how to handle file type: $mimeType", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                    return
                }
            }

        }
    }

    private fun restoreData(inStream: InputStream) {
        val reader = BufferedReader(InputStreamReader(Objects.requireNonNull(inStream)))

        // Valid header?
        try {

            val WEIGHT = 1
            val BLOODPRESSURE = 2
            val DIARY = 3
            val WATER = 4

            val line = reader.readLine()
            if (line == null) {
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.eNoMediLogFile), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return
            }

            // Find out what we are importing
            var selector = -1
            if (line.indexOf(this.getString(string.weight)) > 0) selector = WEIGHT
            if (line.indexOf(this.getString(string.systolic)) > 0) selector = BLOODPRESSURE
            if (line.indexOf(this.getString(string.diary)) > 0) selector = DIARY
            if (line.indexOf(this.getString(string.water)) > 0) selector = WATER

            // Validate header line
            val headerLine = line.replace("\\s".toRegex(), "") // Remove potential blanks

            var importDelimiter = ""
            for (testDelimiter in getString(string.csvSeparators)) {
                if (headerLine.indexOf(testDelimiter) > 0) importDelimiter = testDelimiter.toString()
            }

            // Check if separator exists in headerLine
            if (importDelimiter.isEmpty()) {
                var filterHint = ""
                for (c in getString(string.csvSeparators)) filterHint = "$filterHint$c " // Add blanks for better visibility
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.noCsvDelimiterFound) + " $filterHint", 6000).setAction("Action", null).show()
                return
                }
            else { // Set importDelimiter as default
                val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
                val editor = sharedPref.edit()
                editor.putString(SettingsActivity.KEY_PREF_DELIMITER, importDelimiter)
                editor.apply()
            }

            val wantedHeader: String
            val vm: DataViewModel?
            when (selector) {
                WATER -> {
                    vm = waterViewModel
                    wantedHeader = getString(string.date) + importDelimiter + getString(string.water)
                }
                WEIGHT -> {
                    vm = weightViewModel
                    wantedHeader = getString(string.date) + importDelimiter + getString(string.weight)
                }
                BLOODPRESSURE -> {
                    vm = bloodPressureViewModel
                    wantedHeader = getString(string.date) + importDelimiter + getString(string.systolic) + importDelimiter + getString(string.diastolic) + importDelimiter + getString(string.pulse)
                }
                DIARY -> {
                    vm = diaryViewModel
                    wantedHeader = getString(string.date) + importDelimiter + getString(string.diary)
                }
                else -> {
                    Snackbar.make(findViewById(android.R.id.content), this.getString(string.eNoMediLogFile) + " " + line, 6000).setAction("Action", null).show()
                    return
                }
            }

            if ( headerLine.indexOf(wantedHeader) < 0) { // Wanted header not found = problem with the header
                Snackbar.make(findViewById(android.R.id.content), this.getString(string.errorInCSVHeader) + ": '" + wantedHeader + "', " + this.getString(string.found) + ": '" + headerLine + "'", 6000).setAction("Action", null).show()
                return
            }
            if (vm.getSize(false) > 0) { // No need for warning if the db is empty
                MaterialDialog(this).show {
                    title(string.warning)
                    message(text = getString(string.doYouReallyWantToContinue1) + " " + vm.getSize(false) + " " + getString(string.doYouReallyWantToContinue2) + " " + vm.itemName + " " + getString(string.doYouReallyWantToContinue3))
                    negativeButton(string.cancel)
                    positiveButton(string.yes) {
                        if (vm.suspendedFileImport(reader, true)> 0) setFilterIcon() // Make sure filter is reset
                    }
                }
            } else if (vm.suspendedFileImport(reader, true)> 0) setFilterIcon() // Make sure filter is reset
        }
        catch (e: IOException) {
            Snackbar.make(findViewById(android.R.id.content), this.getString(string.eReadError), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            //Toast.makeText(this, this.getString(string.eReadError), Toast.LENGTH_SHORT).show()
//            Log.e("Exception", "File read failed: $e")
            return
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun createZIP(uri: Uri, zipPassword: String ): DocumentFile? {
        //######################### ZIP ##################################
//        Log.e("Debug", "Folder: $uri")

        // Create empty Zip file
        val zipFile = "MediLog-Backup-" + SimpleDateFormat("yyyy-MM-dd").format(Date().time) + ".zip"
        var dFile: DocumentFile? = null
        try {
            val dFolder = DocumentFile.fromTreeUri(this, uri)
            //             Log.e("Debug", "isDirectory: " + dFolder.isDirectory());
            if (dFolder != null) dFile = dFolder.createFile("application/zip", zipFile)
            if (dFile == null) {
                Toast.makeText(this, this.getString(string.eCreateFile) + ": " + dFile, Toast.LENGTH_LONG).show()
                Log.e("Debug", "ieFile: $dFile")
                return null
            }
        } catch (e: Exception) {
            Toast.makeText(this, this.getString(string.eCreateFile) + ": " + dFile + " " + e.toString(), Toast.LENGTH_LONG).show()
            Log.e("Exception", "Can't create file: $e")
            return null
        }

//        dFile = DocumentFile.fromTreeUri(this, uri);

        // Add files to ZIP file
        val dest: OutputStream?
        val out: ZipOutputStream?

        try {
            dest = contentResolver.openOutputStream(dFile.uri)
            if (dest == null) {
                Toast.makeText(this, this.getString(string.eCreateFile) + " " + dFile, Toast.LENGTH_LONG).show()
                return null
            }
            //           Log.e("Debug", "FileOutputStream: $dest")
            out = if (zipPassword.isEmpty()) ZipOutputStream(BufferedOutputStream(dest))
            else ZipOutputStream(BufferedOutputStream(dest), zipPassword.toCharArray())

//            Log.e("Debug", "ZipOutStream: $out")
//            Log.v("Compress", "Adding: ")
        } catch (e: Exception) {
            Toast.makeText(this, this.getString(string.eCreateFile) + " " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return null
        }
        val zipParameters = ZipParameters()
        if (zipPassword.isNotEmpty()) {
            zipParameters.isEncryptFiles = true
            zipParameters.encryptionMethod = EncryptionMethod.AES
        }

        val fileNames = arrayOfNulls<String>(4)
        fileNames[0] = weightViewModel.fileName
        fileNames[1] = bloodPressureViewModel.fileName
        fileNames[2] = diaryViewModel.fileName
        fileNames[3] = waterViewModel.fileName

        val zipData = arrayOfNulls<String>(4)
        zipData[0] = weightViewModel.getCsvData(false)
        zipData[1] = bloodPressureViewModel.getCsvData(false)
        zipData[2] = diaryViewModel.getCsvData(false)
        zipData[3] = waterViewModel.getCsvData(false)
        for (i in zipData.indices) {
            try {
                zipParameters.fileNameInZip = fileNames[i]
                out.putNextEntry(zipParameters)
                out.write(zipData[i]!!.toByteArray())
                out.closeEntry()
            } catch (e: Exception) {
                Toast.makeText(this, this.getString(string.eCreateFile) + " " + fileNames[i], Toast.LENGTH_LONG).show()
                e.printStackTrace()
                return null
            }
        }
        try {
            out.close()
        } catch (e: Exception) {
            Toast.makeText(this, this.getString(string.eCreateFile) + " 2 " + dFile, Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return null
        }
        if (zipPassword.isEmpty()) {
            Toast.makeText(this, this.getString(string.unprotectedZipFileCreated) + " " + dFile.name, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, this.getString(string.protectedZipFileCreated) + " " + dFile.name, Toast.LENGTH_LONG).show()
        }

        // Done with Backup
        return dFile
    }

    private fun openZIP(zipUri: Uri, zipPassword: String?): Boolean {
        var localFileHeader: LocalFileHeader?
        var readLen: Int
        val readBuffer = ByteArray(4096)

        val zipInputStream = contentResolver.openInputStream(zipUri)

        // Try to open ZIP file
        val zipIn: ZipInputStream
        // With default password from settings
        try {
            zipIn = ZipInputStream(zipInputStream, zipPassword?.toCharArray())
            localFileHeader = zipIn.nextEntry
        }
        catch (e: Exception) {
            Toast.makeText(this, this.getString(string.eOpenZipFile) + e.message, Toast.LENGTH_LONG).show()
            return false
        }

        val filePath = File(externalCacheDir, "")

//        localFileHeader = zipIn.getNextEntry()
        while (localFileHeader != null) {
            val extractedFile = File(filePath,localFileHeader.fileName)
            val outputStream = FileOutputStream(extractedFile)

            readLen = zipIn.read(readBuffer)
            while (readLen != -1) {
                outputStream.write(readBuffer, 0, readLen)
                readLen = zipIn.read(readBuffer)
            }
            outputStream.close()

            val csvStream = FileInputStream(extractedFile)
            restoreData(csvStream)
            localFileHeader = zipIn.nextEntry
        }
        return true
    }


    // #####################################################
    // Application independent code
    // #####################################################
    public override fun onPause() {
        super.onPause()
        // Save active tab
//        Log.d("OnPause", "activeTab " + tabLayout!!.selectedTabPosition)
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = sharedPref.edit()
        editor.putInt("activeTab", tabLayout.selectedTabPosition)
        editor.apply()
    }

    public override fun onStop() {
        super.onStop()
        // Save active tab
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = sharedPref.edit()
        editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
        editor.apply()
    }

    private fun setTheme() {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        when (sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(string.blue))) {
            this.getString(string.green) -> theme.applyStyle(style.AppThemeGreenNoBar, true)
            this.getString(string.blue) -> theme.applyStyle(style.AppThemeBlueNoBar, true)
            this.getString(string.red) -> theme.applyStyle(style.AppThemeRedNoBar, true)
        }
    }

    private fun setFilterIcon() {
        invalidateOptionsMenu()
    }

    companion object {
        private const val REQUEST = 112
        private fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (context != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false
                    }
                }
            }
            return true
        }

        fun resetReAuthenticationTimer(context: Context?) {
            // Reset Stopwatch, after all we are still in MediLog
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = sharedPref.edit()
            editor.putLong("STOPWATCH", Calendar.getInstance().timeInMillis) // Set Re-authentication timer
            editor.apply()
        }

    }
}