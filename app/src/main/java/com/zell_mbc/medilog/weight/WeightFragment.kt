package com.zell_mbc.medilog.weight

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.MainActivity
import com.zell_mbc.medilog.R
import com.zell_mbc.medilog.SettingsActivity
import com.zell_mbc.medilog.Weight
import kotlinx.android.synthetic.main.weight_tab.*
import java.text.DateFormat
import java.util.*

class WeightFragment : Fragment(), WeightListAdapter.ItemClickListener {
    private lateinit var viewModel: WeightViewModel
    private var adapter: WeightListAdapter? = null
    private var quickEntry = true

/*    private fun tmpListener() {
        val tmp = viewModel.filterChanged.value?.plus(1L)
        viewModel.filterChanged.postValue(tmp)
        Log.d("tmpListener : ", "New value: " + viewModel.filterChanged.value)
    }
*/
    private fun addItem() {

        if (!quickEntry) {
            val tmpComment = "temporaryEntry-YouShouldNeverSeeThis"
            val tmpItem = Weight(0, Date().time, tmpComment, 0f)
            val _id = viewModel.insert(tmpItem)
            if (_id  != 0L) editItem(_id.toInt())
            else Snackbar.make(requireView(), "No entry with tmp weight found!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Check empty variables
        val weightValue = etWeight.text.toString()
        val commentValue = etComment.text.toString()
        if (weightValue.isEmpty()) {
            Snackbar.make(requireView(), getString(R.string.weightMissing), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }
        var w = 0f
        try {
            w = weightValue.toFloat()
            if (w <= 0f) {
                Snackbar.make(requireView(), this.getString(R.string.eInvalidValue1) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.eInvalidValue2) + " $w", Snackbar.LENGTH_LONG).setAction("Action", null).show()
                return
            }
        } catch (e: Exception) {
            Snackbar.make(requireView(), "Exception: " + this.getString(R.string.eInvalidValue1) + " " + this.getString(R.string.weight) + " " + this.getString(R.string.eInvalidValue2) + " $w", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }
        val item = Weight(0, Date().time, commentValue, w)

        viewModel.insert(item)

        if ((viewModel.filterEnd > 0L) && (viewModel.filterEnd < item.timestamp)) Snackbar.make(requireView(), getString(R.string.filteredOut), 4000).setAction("Action", null).show()

        etWeight.setText("")
        etComment.setText("")
    }


    private fun editItem(index: Int) {
        val intent = Intent(requireContext(), WeightEditActivity::class.java)
        val extras = Bundle()
        val item = viewModel.getItem(index)
        if (item != null) {
            extras.putInt("ID", item._id)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun deleteItem(index: Int) {
        // Remove from array
//        Log.d("Debug WeightFragment", "Delete: " + index)
        val item = viewModel.getItem(index)
        if (item != null) {
            viewModel.delete(item._id)
        }
        Snackbar.make(requireView(), viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_deleted), Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.weight_tab, parent, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
 //       Log.d("WeightFragment onViewCreated : ", "Start")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(requireContext())
        quickEntry = sharedPref.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val addButton = btAddWeight as FloatingActionButton
//        val tmpButton = btTmp as FloatingActionButton

        when (colourStyle) {
            this.getString(R.string.green) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }
        addButton.setColorFilter(Color.WHITE)

        if (!quickEntry) { // Hide quick entry fields
            etWeight.visibility = View.GONE
            etComment.visibility = View.GONE
            text_unit.visibility = View.GONE
        }
        else {
            etWeight.visibility = View.VISIBLE
            etComment.visibility = View.VISIBLE
            text_unit.visibility = View.VISIBLE
            text_unit.text = sharedPref.getString(SettingsActivity.KEY_PREF_WEIGHTUNIT, "kg")
        }

        val layoutManager = LinearLayoutManager(requireContext())
        rvWeightList.layoutManager = layoutManager

        adapter = WeightListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(WeightViewModel::class.java)
        viewModel.items.observe(requireActivity(), Observer { weight -> weight?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        rvWeightList.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(rvWeightList.context, layoutManager.orientation)
        rvWeightList.addItemDecoration(dividerItemDecoration)

        // Respond to click events
        addButton.setOnClickListener { addItem() }

        val showChartButton = button_showWeightChart as FloatingActionButton
        when (colourStyle) {
            this.getString(R.string.green) -> showChartButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightGreen))
            this.getString(R.string.blue) -> showChartButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightBlue))
            this.getString(R.string.red) -> showChartButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorLightRed))
        }

        showChartButton.setOnClickListener(View.OnClickListener {
            val c = context
            if (c == null) {
                Log.d("Debug OpenChart: ", "Empty Context")
                return@OnClickListener
            }
            if (viewModel.getSize(true) < 2) {
                Snackbar.make(view, c.getString(R.string.notEnoughDataForChart), Snackbar.LENGTH_LONG).setAction("Action", null).show()
                //               Toast.makeText(c, c.getString(R.string.notEnoughDataForChart), Toast.LENGTH_LONG).show()
                return@OnClickListener
            }

            val intent = Intent(requireContext(), WeightChartActivity::class.java)
            startActivity(intent)

/*            requireActivity().supportFragmentManager.beginTransaction().replace(android.R.id.content, WeightChartFragment()).commit()
            val nextFrag = WeightChartFragment()
            requireActivity().supportFragmentManager.beginTransaction()
                    .replace(((view as ViewGroup).parent as View).id, nextFrag, "WeightChartFragment")
                    .addToBackStack(null)
                    .commit()*/
        })
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        MaterialDialog(requireContext()).show {
            title(text = getString(R.string.ItemClicked) + " " + dateFormat.format(item.timestamp))
            message(R.string.whatDoYouWant)
            neutralButton(R.string.cancel)
            positiveButton(R.string.delete) { deleteItem(item._id) }
            negativeButton(R.string.edit) { editItem(item._id) }
        }
        MainActivity.resetReAuthenticationTimer(requireContext()) // User is interacting so let's reset the timeout
    }
}