package com.zell_mbc.medilog.weight

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.zell_mbc.medilog.base.DataRepository
import com.zell_mbc.medilog.Weight

class WeightRepository(private val dao: WeightDao, fS:Long, fE: Long): DataRepository() {

        init {
            filterStart = fS
            filterEnd = fE
        }

        fun getItems(filterStart: Long, filterEnd: Long): LiveData<List<Weight>> {
                return if ((filterStart != 0L) && (filterEnd == 0L)) dao.getItemsGreaterThan(filterStart)
                else if ((filterStart == 0L) && (filterEnd != 0L)) dao.getItemsLessThan(filterEnd)
                else if ((filterStart != 0L) && (filterEnd != 0L)) dao.getItemsRange(filterStart, filterEnd)
                else dao.getItems()
        }

        override fun getItems(order: String, filterStart: Long, filterEnd: Long): List<Weight> {
                if (order == "ASC") {
                        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getWeightItemsASCGreaterThan(filterStart)
                        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getWeightItemsASCLessThan(filterEnd)
                        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getWeightItemsASCRange(filterStart, filterEnd)
                        return dao.getWeightItemsASC()
                } else {
                        if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getWeightItemsDESCGreaterThan(filterStart)
                        if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getWeightItemsDESCLessThan(filterEnd)
                        if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getWeightItemsDESCRange(filterStart, filterEnd)
                        return dao.getWeightItemsDESC()
                }
        }
/*
                var tmpFilterEnd = 0L
                if (filterEnd == 0L) tmpFilterEnd = Calendar.getInstance().timeInMillis + (86400000 * 100) // Add a 100 days worth of milliseconds to make sure we capture all of todays items
                else tmpFilterEnd = filterEnd
                if (order.equals("ASC")) return dao.getWeightItemsASCFiltered(filterStart, filterEnd)
                else return dao.getWeightItemsDESCFiltered(filterStart, tmpFilterEnd) */

        override fun getSize(filterStart: Long, filterEnd: Long): Int {
                if ((filterStart != 0L) && (filterEnd == 0L)) return dao.getSizeGreaterThan(filterStart)
                if ((filterStart == 0L) && (filterEnd != 0L)) return dao.getSizeLessThan(filterEnd)
                if ((filterStart != 0L) && (filterEnd != 0L)) return dao.getSizeRange(filterStart, filterEnd)
                return dao.getSize()
        }

        override fun getItem(index: Int): Weight = dao.getItem(index)

        fun getMin(): Float = dao.getMinWeight()
        fun getMax(): Float = dao.getMaxWeight()

        suspend fun insert(item: Weight): Long { return dao.insert(item) }
        suspend fun update(item: Weight) { dao.update(item) }
        suspend fun delete(item: Weight) { dao.delete(item) }

        override suspend fun delete(tmpComment: String) {
                dao.delete(tmpComment)
        }

        override suspend fun delete(id: Int) {
                dao.delete(id)
        }

        override suspend fun deleteAll() {
                dao.deleteAll()
        }

        override suspend fun deleteAllFiltered(filterStart: Long, filterEnd: Long) {
                dao.deleteAllFiltered(filterStart, filterEnd)
        }

}