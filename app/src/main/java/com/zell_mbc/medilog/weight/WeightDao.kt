package com.zell_mbc.medilog.weight

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.zell_mbc.medilog.Weight
import com.zell_mbc.medilog.base.DataDao

@Dao
abstract class WeightDao: DataDao<Weight> {

    @Query("DELETE from weight where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from weight where comment = :tmpComment")
    abstract suspend fun delete(tmpComment: String)

    @Query("SELECT MIN(weight) FROM weight")
    abstract fun getMinWeight(): Float

    @Query("SELECT MAX(weight) FROM weight")
    abstract fun getMaxWeight(): Float

    // ----------------------------
    @Query("SELECT COUNT(*) FROM weight")
    abstract fun getSize(): Int

    @Query("SELECT COUNT(*) FROM weight WHERE timestamp >=:start")
    abstract fun getSizeGreaterThan(start: Long): Int

    @Query("SELECT COUNT(*) FROM weight WHERE timestamp <=:end")
    abstract fun getSizeLessThan(end: Long): Int

    @Query("SELECT COUNT(*) FROM weight WHERE timestamp >=:start and timestamp <=:end")
    abstract fun getSizeRange(start: Long, end: Long): Int
    // ----------------------------

    @Query("SELECT * from weight where _id = :id")
    abstract fun getItem(id: Int): Weight

    // ----------------------------
    // getWeightItems options, return type List
    // ------------ASC ------------
    @Query("SELECT * from weight ORDER BY timestamp ASC") // Used by Chart fragment
    abstract fun getWeightItemsASC(): List<Weight>

    @Query("SELECT * from weight WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getWeightItemsASCRange(start: Long, end: Long): List<Weight>

    @Query("SELECT * from weight WHERE timestamp >=:start ORDER BY timestamp ASC")
    abstract fun getWeightItemsASCGreaterThan(start: Long): List<Weight>

    @Query("SELECT * from weight WHERE timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getWeightItemsASCLessThan(end: Long): List<Weight>

    // ------------DESC ------------
    @Query("SELECT * from weight ORDER BY timestamp DESC")
    abstract fun getWeightItemsDESC(): List<Weight>

    @Query("SELECT * from weight WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getWeightItemsDESCRange(start: Long, end: Long): List<Weight>

    @Query("SELECT * from weight WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getWeightItemsDESCGreaterThan(start: Long): List<Weight>

    @Query("SELECT * from weight WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getWeightItemsDESCLessThan(end: Long): List<Weight>

    // ----------------------------
    // getWeightItems options, return type LiveData
    // Used by Recycler view
    @Query("SELECT * from weight WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsRange(start: Long, end: Long): LiveData<List<Weight>>

    @Query("SELECT * from weight WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsGreaterThan(start: Long): LiveData<List<Weight>>

    @Query("SELECT * from weight WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsLessThan(end: Long): LiveData<List<Weight>>

    @Query("SELECT * from weight ORDER BY timestamp DESC")
    abstract fun getItems(): LiveData<List<Weight>>

    // ----------------------

    @Query("DELETE FROM weight")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM weight WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)
}
