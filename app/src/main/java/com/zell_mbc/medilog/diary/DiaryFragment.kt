package com.zell_mbc.medilog.diary

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import androidx.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.zell_mbc.medilog.*
import kotlinx.android.synthetic.main.diary_tab.*
import java.util.*
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import java.text.DateFormat

class DiaryFragment : Fragment(), DiaryListAdapter.ItemClickListener {
    private lateinit var viewModel: DiaryViewModel
    private var adapter: DiaryListAdapter? = null
    private var quickEntry = true

    private val good = 0
    private val notGood = 1
    private val bad = 2
    private var state = good

    private var standardColors = 0 // : Int //ColorStateList


    private fun addItem() {
        if (!quickEntry) {
            val tmpComment = "temporaryEntry-YouShouldNeverSeeThis"
            val tmpItem = Diary(0, Date().time, tmpComment, "",0)
            val _id = viewModel.insert(tmpItem)
            if (_id  != 0L) editItem(_id.toInt())
            else Snackbar.make(requireView(), "No entry with tmp weight found!", Snackbar.LENGTH_LONG).setAction("Action", null).show()
            return
        }

        // Check empty variables
        val value = etDiary.text.toString()
        if (value.isEmpty()) {
            val v = view
            if (v != null) {
                Snackbar.make(v, getString(R.string.diaryMissing), Snackbar.LENGTH_LONG).setAction("Action", null).show()
            }
            return
        }

        val item = Diary(0, Date().time, "", value,state)
        viewModel.insert(item)
        if ((viewModel.filterEnd > 0 ) && (viewModel.filterEnd < item.timestamp)) Snackbar.make(requireView(), getString(R.string.filteredOut), 4000).setAction("Action", null).show()
//        else Snackbar.make(requireView(), getString(R.string.word_new) + " " + viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_saved), Snackbar.LENGTH_LONG).setAction("Action", null).show()

        state = good
        btSetColour.setTextColor(standardColors)
        etDiary.setText("")
    }

    private fun editItem(index: Int) {
        val intent = Intent(requireContext(), DiaryEditActivity::class.java)
        val extras = Bundle()
        val item = viewModel.getItem(index)
        if (item != null) {
            extras.putInt("ID", item._id)
            intent.putExtras(extras)
            startActivity(intent)
        } // Save id for lookup by EditFragment
    }

    private fun deleteItem(index: Int) {
        val item = viewModel.getItem(index)
        if (item != null) {
            viewModel.delete(item._id)
        }
        Snackbar.make(requireView(), viewModel.itemName + " " + getString(R.string.word_item) + " " + getString(R.string.word_deleted), Snackbar.LENGTH_LONG).setAction("Action", null).show()
    }

    private fun setColour() {
        if (state == bad) state = good
        else              state++

        when (state) {
            good    -> btSetColour.setTextColor(standardColors)
            notGood -> btSetColour.setTextColor(Color.rgb(255, 191, 0)) //backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryAmber))
            bad     -> btSetColour.setTextColor(Color.RED) //backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }
    }

    fun notifyDataSetChanged() {
        adapter?.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.diary_tab, parent, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))
        val addButton = btAddDiary as FloatingActionButton
//        val setColourButton = btSetColour as FloatingActionButton
        quickEntry = sharedPref.getBoolean(SettingsActivity.KEY_PREF_QUICKENTRY, true)

        when (colourStyle) {
            this.getString(R.string.green) -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryGreen))
            this.getString(R.string.blue)  -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryBlue))
            this.getString(R.string.red)   -> addButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorPrimaryRed))
        }
        addButton.setColorFilter(Color.WHITE)

        standardColors =  etDiary.currentHintTextColor //  textColors.defaultColor
        btSetColour.setTextColor(standardColors) // Make sure the button comes up in correct colour
//        Log.d("DiaryFragment onCreateView : ", "standardColors " + standardColors )

        // Hide quick entry fields if needed
        if (!quickEntry) {
            etDiary.visibility = View.GONE
            btSetColour.visibility = View.GONE
        }
        else etDiary.visibility = View.VISIBLE

        // set up the RecyclerView
        val layoutManager = LinearLayoutManager(requireContext())
        rvDiaryList.layoutManager = layoutManager

        adapter = DiaryListAdapter(requireContext())
        viewModel = ViewModelProvider(requireActivity()).get(DiaryViewModel::class.java)
        viewModel.items.observe(requireActivity(), Observer { diary -> diary?.let { adapter!!.setItems(it) } })

        adapter!!.setClickListener(this)
        rvDiaryList.adapter = adapter

        // Respond to click events
        addButton.setOnClickListener { addItem() }
        btSetColour.setOnClickListener { setColour() }
    }

    override fun onItemClick(view: View?, position: Int) {
        val item = adapter?.getItemAt(position)
        item ?: return
        val dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        MaterialDialog(requireContext()).show {
            title(text = getString(R.string.ItemClicked) + " " + dateFormat.format(item.timestamp))
            message(R.string.whatDoYouWant)
            neutralButton(R.string.cancel)
            positiveButton(R.string.delete) { deleteItem(item._id) }
            negativeButton(R.string.edit) { editItem(item._id) }
        }
    }
}