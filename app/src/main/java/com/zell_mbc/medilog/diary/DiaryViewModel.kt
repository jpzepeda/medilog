package com.zell_mbc.medilog.diary

import android.app.Application
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.pdf.PdfDocument
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.*
import com.zell_mbc.medilog.base.DataViewModel
import kotlinx.coroutines.*
import java.io.BufferedReader
import java.io.IOException
import java.lang.StringBuilder
import java.text.DateFormat
import java.text.ParseException
import java.util.*

class DiaryViewModel(application: Application): DataViewModel(application) {
    override val fileName = "Diary.csv"
    override var itemName = app.getString(R.string.diary)
    override val filterStartPref = "DIARYFILTERSTART"
    override val filterEndPref = "DIARYFILTEREND"

    override lateinit var repository: DiaryRepository
    lateinit var dao: DiaryDao
    lateinit var items: LiveData<List<Diary>>

    override fun init() {
        super.init()

        dao = MediLogDB.getDatabase(app).diaryDao()
        repository = DiaryRepository(dao, filterStart, filterEnd)
        items = Transformations.switchMap(filterChanged) { repository.getItems(filterStart, filterEnd) }
    }

    fun insert(item: Diary): Long {
        var rowId = -1L
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            rowId = repository.insert(item)
//            rowId = repository.getIdFromRowId(tmpL) // Translate from DB row to _id column
        }
            j.join() }
        return rowId
    }

    fun update(diary: Diary) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(diary)
    }

    fun delete(diaryValue: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(diaryValue)
    }

    override fun getItem(id: Int): Diary? {
        var item: Diary? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(id) }
            j.join() }
        return item
    }

    fun getItem(diaryValue: String): Diary? {
        var item: Diary? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(diaryValue) }
            j.join() }
        return item
    }

    override fun getItems(order: String, filtered: Boolean): List<Diary> {
        if (filtered) getFilter()
        lateinit var items: List<Diary>
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            items = repository.getItems(order, filterStart, filterEnd) }
            j.join() }
        return items
    }

// --------


    override suspend fun importFile(reader: BufferedReader?, replaceData: Boolean): Int {
        val tmpItems = ArrayList<Diary>()
        if (reader == null) {
            return 0
        }
        try {
            var showError = true
            var line: String?
            var lineNo = 0
            while (reader.readLine().also { line = it } != null) {
                val item = Diary(0, 0,"", "",0) // Start with empty item
                val err = csvToItem(this, item, line!!)
                lineNo++
                if (err < 0) {
                    if (showError) {
                        Handler(Looper.getMainLooper()).post {
                            Toast.makeText(app, "Error $err importing line $lineNo, '$line'", Toast.LENGTH_LONG).show()
                        }
                        showError = false // Show only first error to avoid pushing hundreds of similar errors
                    }
                    continue
                }
                tmpItems.add(item)
            }
        } catch (e: IOException) {
            return -1
        }

        return transferItems(tmpItems, replaceData)
    }


    private fun transferItems(tmpItems: ArrayList<Diary>, replaceData: Boolean): Int {
        val records = tmpItems.size
        if (records == 0) return -2     // Delete only after a successful import

        // Delete only after a successful import
        if (replaceData) {
            runBlocking { deleteAll(false) }
        }

        for (item in tmpItems) insert(item)
        return records
    }


    override fun getCsvData(filtered: Boolean): String {
        val sb = StringBuilder()

        // CSV header
        sb.append(app.getString(R.string.date) + separator + app.getString(R.string.diary) + separator + app.getString(R.string.state) + System.getProperty("line.separator"))

        // Data
        val items = getItems("DESC", filtered)
        for (item in items) {
            sb.append(csvPattern.format(item.timestamp) + separator + item.diary + separator + item.state + separator + System.getProperty("line.separator"))
            }
        return sb.toString()
    }

    override fun createPdfDocument(filtered: Boolean): PdfDocument? {
        if (getSize(filtered) == 0) {
            Toast.makeText(app, app.getString(R.string.diary) + " " + app.getString(R.string.noDataToExport), Toast.LENGTH_LONG).show()
            return null
        }
        val userName = sharedPref.getString(SettingsActivity.KEY_PREF_USER, "")
        val document = PdfDocument()


//        int tab1 = 80;
        val pdfDataTab = pdfTimeTab + 70
        var pageNumber = 1
        var i: Int
        var pageInfo: PdfDocument.PageInfo?
        var page: PdfDocument.Page
        var canvas: Canvas

        val formatedDate = DateFormat.getDateInstance(DateFormat.SHORT).format(Calendar.getInstance().time) + " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(Calendar.getInstance().time)

        val pdfPaint = Paint()
        pdfPaint.color = Color.BLACK
        val paintRed = Paint()
        paintRed.color = Color.RED
        val pdfPaintHighlight = Paint()
        pdfPaintHighlight.isFakeBoldText = true
        val pdfPaintItalics = Paint()
        pdfPaintItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        val pdfPaintBoldItalics = Paint()
        pdfPaintBoldItalics.typeface = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)
        pdfPaintBoldItalics.isFakeBoldText = true

        // crate a A4 page description
        pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
        page = document.startPage(pageInfo)
        canvas = page.canvas

        pdfRightBorder = canvas.width - pdfLeftBorder
        pdfDataBottom = canvas.height - 15

        val pdfHeaderDateColumn = pdfRightBorder - 150

        // Draw header
        var headerText = app.getString(R.string.bpReportTitle)
        if (userName != null) headerText = headerText + " " + app.getString(R.string.forString) + " " + userName

        //                Log.d("--------------- Debug", "HeaderText:" + headerText);
        canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
        canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)

        i = pdfDataTop
        // Print header in bold
        canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaintHighlight)
        canvas.drawText(app.getString(R.string.diary), pdfDataTab.toFloat(), i.toFloat(), pdfPaintHighlight)

        // ------------
        // Todo: Line breaks
        val items = getItems("ASC", filtered)
        for (item in items) {
            i += pdfLineSpacing
            // Start new page
            if (i > pdfDataBottom) {
                document.finishPage(page)
                pageNumber++

                // crate a A4 page description
                pageInfo = PdfDocument.PageInfo.Builder(595, 842, pageNumber).create()
                page = document.startPage(pageInfo)
                canvas = page.canvas
                canvas.drawText(headerText, pdfLeftBorder.toFloat(), pdfHeaderDateColumn.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.date) + ": " + formatedDate, pdfHeaderDateColumn.toFloat(), pdfHeaderTop.toFloat(), pdfPaint)
                canvas.drawLine(pdfLeftBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfRightBorder.toFloat(), pdfHeaderBottom.toFloat(), pdfPaint)
                canvas.drawLine(pdfLeftBorder.toFloat(), pdfDataBottom.toFloat(), pdfRightBorder.toFloat(), pdfDataBottom.toFloat(), pdfPaint)
                i = pdfDataTop
                canvas.drawText(app.getString(R.string.date), pdfLeftBorder.toFloat(), i.toFloat(), pdfPaint)
                canvas.drawText(app.getString(R.string.diary), pdfDataTab.toFloat(), i.toFloat(), pdfPaint)
                // ------------
                i += pdfLineSpacing
            }
            var paint = pdfPaint
            if (item.state == 1 ) paint = pdfPaintItalics
            if (item.state == 2 ) paint = pdfPaintBoldItalics
            canvas.drawText(toStringDate(item.timestamp), pdfLeftBorder.toFloat(), i.toFloat(), paint)
            canvas.drawText(item.diary, pdfDataTab.toFloat(), i.toFloat(), paint)
        }
        // finish the page
        document.finishPage(page)
        return document
    }

    companion object {
        private fun csvToItem(diaryViewModel: DiaryViewModel, w: Diary, csv: String): Int {
            val data = csv.split(diaryViewModel.separator!!.toRegex()).toTypedArray()
    
            // At a minimum 2 values need to be present, comment is optional
            if (data.size < 2) {
                return -1
            }
            try {
                val date: Date? = diaryViewModel.csvPattern.parse(data[0].trim())
                if (date != null) w.timestamp =  date.time
            } catch (e: ParseException) {
                return -2
            }
            try {
                w.diary = data[1]
            } catch (e: NumberFormatException) {
                return -3
            }
            if (data.size > 2) { // Only try to import if a field exists
                try {
                    w.state = data[2].toInt()
                } catch (e: NumberFormatException) {
                    return -4
                }
            }
            else w.state = 0
    
            // Comment not always available
            w.comment = if (data.size == 4) {
                data[3]
            } else {
                ""
            }
            return 0
        }
    }
}
