package com.zell_mbc.medilog.diary

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.zell_mbc.medilog.Diary
import com.zell_mbc.medilog.base.DataDao

@Dao
abstract class DiaryDao: DataDao<Diary> {

    @Query("DELETE from diary where _id = :id")
    abstract suspend fun delete(id: Int)

    @Query("DELETE from diary where diary = :diaryValue")
    abstract suspend fun delete(diaryValue: String)

    // ----------------------------
    @Query("SELECT COUNT(*) FROM diary")
    abstract fun getSize(): Int

    @Query("SELECT COUNT(*) FROM diary WHERE timestamp >=:start")
    abstract fun getSizeGreaterThan(start: Long): Int

    @Query("SELECT COUNT(*) FROM diary WHERE timestamp <=:end")
    abstract fun getSizeLessThan(end: Long): Int

    @Query("SELECT COUNT(*) FROM diary WHERE timestamp >=:start and timestamp <=:end")
    abstract fun getSizeRange(start: Long, end: Long): Int
    // ----------------------------

    @Query("SELECT * from diary where _id = :id")
    abstract fun getItem(id: Int): Diary

    @Query("SELECT * FROM diary WHERE diary = :diaryValue")
    abstract fun getItem(diaryValue: String): Diary

    // ----------------------------
    // getDiaryItems options, return type List
    // ------------ASC ------------
    @Query("SELECT * from diary ORDER BY timestamp ASC") // Used by Chart fragment
    abstract fun getItemsASC(): List<Diary>

    @Query("SELECT * from diary WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getItemsASCRange(start: Long, end: Long): List<Diary>

    @Query("SELECT * from diary WHERE timestamp >=:start ORDER BY timestamp ASC")
    abstract fun getItemsASCGreaterThan(start: Long): List<Diary>

    @Query("SELECT * from diary WHERE timestamp <=:end ORDER BY timestamp ASC")
    abstract fun getItemsASCLessThan(end: Long): List<Diary>

    // ------------DESC ------------
    @Query("SELECT * from diary ORDER BY timestamp DESC") // Used by Recycler view
    abstract fun getItemsDESC(): List<Diary>

    @Query("SELECT * from diary WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsDESCRange(start: Long, end: Long): List<Diary>

    @Query("SELECT * from diary WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsDESCGreaterThan(start: Long): List<Diary>

    @Query("SELECT * from diary WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsDESCLessThan(end: Long): List<Diary>

    // ----------------------------
    // getDiaryItems options, return type LiveData
    // Recycler view
    @Query("SELECT * from diary WHERE timestamp >=:start and timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsRange(start: Long, end: Long): LiveData<List<Diary>>

    @Query("SELECT * from diary WHERE timestamp >=:start ORDER BY timestamp DESC")
    abstract fun getItemsGreaterThan(start: Long): LiveData<List<Diary>>

    @Query("SELECT * from diary WHERE timestamp <=:end ORDER BY timestamp DESC")
    abstract fun getItemsLessThan(end: Long): LiveData<List<Diary>>

    @Query("SELECT * from diary ORDER BY timestamp DESC")
    abstract fun getItems(): LiveData<List<Diary>>
    // ----------------------

    @Query("DELETE FROM diary")
    abstract suspend fun deleteAll()

    @Query("DELETE FROM diary WHERE timestamp >=:start and timestamp <=:end")
    abstract suspend fun deleteAllFiltered(start: Long, end: Long)
}
