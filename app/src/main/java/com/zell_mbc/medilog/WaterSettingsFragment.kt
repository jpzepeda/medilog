package com.zell_mbc.medilog

import android.os.Bundle
import com.takisoft.preferencex.PreferenceFragmentCompat

class WaterSettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.waterpreferences, rootKey)
    }

    override fun onPause() {
        super.onPause()
        MainActivity.resetReAuthenticationTimer(requireContext())
    }

}