# MediLog
![MediLog](./app/src/main/ic_launcher.png)

Android App to easily capture blood pressure, body weight and new in 1.9.x , daily water intake

## Key features

The main goal of MediLog is not to cover the whole universe of medical data but to provide a solution for cases where the alternatives either don't respect privacy, are full of trackers or complicated.

Guiding principles for MediLog are:

- Be simple. Allow to capture data as quickly as possible to not get in the way. If you can think of ways to improve the UI, let me know.
- Be open. Export and import format is a simple CSV, if you can export CSV from your existing application it should be fairly easy to import into MediLog
- Be secure. PDF reports and ZIP file backups are password protected, the database is encrypted
- Be transparent. No data transfers to anyone but the one you choose. No telemetry, no automatic crash logs, etc.

MediLog will not:

- Connect to your scale or blood pressure device. Data entry is manual
- Add data types which one measures/records infrequently (e.g. once every other week), there are more efifcient ways to capture that data, a piece of paper for example


### Installation

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.zell_mbc.medilog/)

Latest build (including beta builds can be found here: https://gitlab.com/toz12/medilog/-/tree/master/app/release

**Important note:** Be careful with mixing where you install the application from. Different download locations will make Android delete your existing MediLog data during an update. 
Either export your data first or stick with one provider: F-Droid, MediLog Website, compile yourselve

### Removal
Like with all Android applications, via a long click on the app icon.


**Uninstalling the application will delete the database! Unrecoverable! Make sure you export your data first!**

### User manual
Can be found here:https://gitlab.com/toz12/medilog/-/wikis/home 


### Privacy Policy

#### Stored data
Data entered by the user is stored inside an encrypted SQLite database. 

**To keep the input process as simple and fast as possible, the app/your data is not protected with an additional password. If your device has support for Biometric (Fingerprint) make sure to enable it. Otherwise, if someone is able to unlock your device they can access your health data!**

The app supports storing backups in encrypted ZIP files. Make use of itby adding a password whenever you create a file or setting a default one in the settings dialog. Don't forget to test (and remember) the password.
The app allows to send protected files so you can share over unprotected media (eg. email) without the risk of your data getting in the wrong hands.


#### Required permissions

- WRITE_EXTERNAL_STORAGE : Required to export CSV backup files
- READ_EXTERNAL_STORAGE : Required to import CSV files


#### Tracking and Libraries
This application does not use any tracking tools and does not run advertising.

3rd party libraries used are the below:

- AndroidPlot: For the charts
- Takisoft.fix: To fix an Android preference dialog bug (https://github.com/takisoft/preferencex-android#extra-types)
- Zip4J: For handling password protected ZIP files (https://github.com/srikanth-lingala/zip4j)
- SQLCipher: To encrypt the SQLite database (https://www.zetetic.net/sqlcipher/)
- Material-dialogs: For some of the dialog windows (https://github.com/afollestad/material-dialogs)

## Contact

- Matrix: #medilog:zell-mbc.com
- Fediverse: https://social.zell-mbc.com/de/@thomas
- eMail: medilog@zell-mbc.com

## Languages
The number of translations is growing quickly. If yours isn't covered, have a look at https://weblate.bubu1.eu/projects/medilog/core/ or drop me a message and see if you can help the project.

- English
- German
- French (https://framapiaf.org/@djelouze)
- Dutch (https://mastodon.technology/@mondstern)
- Italian (https://mastodon.technology/@mondstern)
- Spanish (https://mastodon.technology/@mondstern)
- Danish (https://mastodon.technology/@mondstern)

Translation platform: https://weblate.bubu1.eu/projects/medilog/core/

A big thank you as well to https://chaos.social/@bubu for providing his translation platform to MediLog

## Donations
If you feel like it…

[<img src="https://www.paypalobjects.com/webstatic/de_DE/i/de-pp-logo-100px.png" border="0"
    alt="PayPal Logo">](https://www.paypal.com/paypalme/thomaszellmbc)

[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bitcoin_logo.svg/252px-Bitcoin_logo.svg.png" border="0"
height="20" alt="Bitcoin Logo">](https://live.blockcypher.com/btc/address/1EUrRpjDAGgpS8J46tmsVWKbgNqAv7rWC7/)



## Screenshots
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/Weight.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/BloodPressure.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/Diary.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/WeightChart.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/BloodPressureChart.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/Settings.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/About.png)
![image](fastlane/metadata/android/en-US/images/phoneScreenshots/PDFReport.png)

## Changelog
[Change log](ChangeLog.md)


