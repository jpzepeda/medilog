## v1.8.1, build 5312
New 
- Added entries for missing French translation items to make F-Droid compile

Fixed
- Fixed a bug which accepted various delimiters during import file header heck check, but expected the set one during the actual import.

Known issues
- Setting/Changing the filter is not reflected in the data tabs right away. Needs a manual screen refresh via launching About or Settings screen or, of course restarting the app
